<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    use HasFactory;

    public function patient()
    {
        return $this->belongsTo(Patient::class);
    }

    public function specialist()
    {
        return $this->belongsTo(User::class, 'specialist_id', 'id');
    }

    public function payments()
    {
        return $this->hasMany(Payment::class, 'appointment_id');
    }

    public function notes()
    {
        return $this->hasMany(Note::class);
    }

    public function getStatusText()
    {
        switch ($this->status){
            case 0:
                $status = 'Pending';
                break;
            case 1:
                $status = 'Completed';
                break;
            case 2:
                $status = 'Canceled';
                break;
            default:
                $status = 'Unknown';
        }
        return $status;
    }


    public function getStatus()
    {
        switch ($this->status){
            case 0:
                $status = '<span class="badge badge-warning p-2">Pending</span>';
                break;
            case 1:
                $status = '<span class="badge badge-primary p-2">Completed</span>';
                break;
            case 2:
                $status = '<span class="badge badge-danger p-2">Canceled</span>';
                break;
            default:
                $status = '<span class="badge badge-dark p-2">Unknown</span>';
        }
        return $status;
    }
}
