<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    use HasFactory;

    public function getType()
    {
        switch ($this->type){
            case 1:
                $type = 'Child';
                break;
            case 2:
                $type = 'Teenager';
                break;
            case 3:
                $type = 'Adult';
                break;
            case 4:
                $type = 'Couples';
                break;
            default:
                $type = 'Unknown';
        }
        return $type;
    }

    public function specialist()
    {
        return $this->belongsTo(User::class);
    }

    public function appointments()
    {
        return $this->hasMany(Appointment::class);
    }
}
