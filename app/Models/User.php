<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function isAdmin()
    {
        if ($this->rank == 1) {
            return true;
        }
        return false;
    }

    public function isSpecialist()
    {
        if ($this->rank == 2) {
            return true;
        }
        return false;
    }

    public function isAssistant()
    {
        if ($this->rank == 3) {
            return true;
        }
        return false;
    }

    public function payments()
    {
        return $this->hasMany(SpecialistPayment::class, 'specialist_id', 'id');
    }

    public function appointments()
    {
        return $this->hasMany(Appointment::class, 'specialist_id');
    }

    public function getRank()
    {
        switch ($this->rank){
            case 1:
                $rank = 'Admin';
                break;
            case 2:
                $rank = 'Specialist';
                break;
            case 3:
                $rank = 'Assistant';
                break;
            default:
                $rank = 'Unknown';
        }
        return $rank;
    }

    public function getStatus()
    {
        switch ($this->status){
            case 1:
                $status = 'Active';
                break;
            case 0:
                $status = 'Passive';
                break;
            default:
                $status = 'Unknown';
        }
        return $status;
    }
}
