<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SpecialistPayment extends Model
{
    use HasFactory;

    public function specialist()
    {
        return $this->belongsTo(User::class, 'specialist_id', 'id');
    }
}
