<?php

namespace App\Providers;

use Blade;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::if('admin', function () {
            return auth()->check() && auth()->user()->isAdmin();
        });

        Blade::if('specialist', function () {
            return auth()->check() && auth()->user()->isSpecialist();
        });

        Blade::if('assistant', function () {
            return auth()->check() && auth()->user()->isAdmin();
        });
    }
}
