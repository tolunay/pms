<?php

namespace App\Http\Livewire;

use App\Models\Patient;
use App\Models\User;
use Livewire\Component;

class AddPatient extends Component
{
    public $patient = 1;
    public $name;
    public $email;
    public $phone;
    public $id_number;
    public $birth_date;

    public $w_name;
    public $w_birth_date;
    public $w_id_number;
    public $w_occupation;
    public $w_phone;
    public $w_email;

    public $m_name;
    public $m_birth_date;
    public $m_id_number;
    public $m_occupation;
    public $m_phone;
    public $m_email;

    public $specialist_id;
    public $success = false;



    public function updatedPatient()
    {
//        dd($this->patient);
    }

    public function store()
    {
        $this->validate([
            'specialist_id' => ['nullable', 'exists:users,id'],
            'name' => ['required', 'string'],
            'email' => ['required', 'email'],
            'phone' => ['required', 'integer', 'unique:patients'],
            'id_number' => ['required', 'integer', 'unique:patients'],
            'birth_date' => ['required', 'date'],
            'w_name' => ['nullable', 'string'],
            'w_birth_date' => ['nullable', 'date'],
            'w_id_number' => ['nullable', 'integer', 'unique:patients,woman_id_number'],
            'w_occupation' => ['nullable', 'string'],
            'w_phone' => ['nullable', 'integer', 'unique:patients,woman_phone'],
            'w_email' => ['nullable', 'email', 'unique:patients,woman_email'],
            'm_name' => ['nullable', 'string'],
            'm_birth_date' => ['nullable', 'date'],
            'm_id_number' => ['nullable', 'integer', 'unique:patients,man_id_number'],
            'm_occupation' => ['nullable', 'string'],
            'm_phone' => ['nullable', 'integer', 'unique:patients,man_phone'],
            'm_email' => ['nullable', 'email', 'unique:patients,man_email']
        ]);

        $patient = new Patient();
        $patient->specialist_id = $this->specialist_id;
        $patient->name = $this->name;
        $patient->email = $this->email;
        $patient->phone = $this->phone;
        $patient->id_number = $this->id_number;
        $patient->type = $this->patient;
        $patient->birth_date = $this->birth_date;
        $patient->woman_name = $this->w_name;
        $patient->woman_birth_date = $this->w_birth_date;
        $patient->woman_id_number = $this->w_id_number;
        $patient->woman_occupation = $this->w_occupation;
        $patient->woman_phone = $this->w_phone;
        $patient->woman_email = $this->w_email;
        $patient->man_name = $this->m_name;
        $patient->man_birth_date = $this->m_birth_date;
        $patient->man_id_number = $this->m_id_number;
        $patient->man_occupation = $this->m_occupation;
        $patient->man_phone = $this->m_phone;
        $patient->man_email = $this->m_email;
        $patient->save();

        $this->reset();
        $this->success = true;
    }

    public function render()
    {
        $specialists = User::where('rank', 2)->get();
        return view('livewire.add-patient', [
            'specialists' => $specialists
        ]);
    }
}
