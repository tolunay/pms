<?php

namespace App\Http\Livewire;

use App\Models\Appointment;
use App\Models\Patient;
use App\Models\User;
use Carbon\Carbon;
use Livewire\Component;

class CreateAppointment extends Component
{
    public $progress = 0;
    public $stepTitle = 'Step 1  - Choose A Patient';
    public $showPatients = false;
    public $showSpecialists = false;
    public $search;
    public $patients;
    public $specialists;
    public $chosenPatient;
    public $chosenSpecialist;
    public $date;
    public $time;
    public $specialistAppointments;
    public $showDatePicker = false;
    public $success = false;

    public function updatedSearch()
    {
        $this->reset(['showSpecialists', 'specialists', 'progress', 'stepTitle', 'showDatePicker', 'date', 'time', 'specialistAppointments']);
        $this->patients = Patient::where('name', 'like', '%' . $this->search . '%')
            ->orWhere('phone', 'like', '%' . $this->search . '%')
            ->orWhere('email', 'like', '%' . $this->search . '%')
            ->orWhere('id_number', 'like', '%' . $this->search . '%')->get();
        $this->showPatients = true;
    }

    public function choosePatient($id)
    {
        $this->chosenPatient = $id;
        $this->showPatients = false;
        $this->specialists = User::where('rank' , 2) -> get();
        $this->showSpecialists = true;
        $this->progress = 33;
        $this->stepTitle = 'Step 2  - Choose A Specialist';
    }

    public function chooseSpecialist($id)
    {
        $this->chosenSpecialist = $id;
        $this->showSpecialists = false;
        $this->progress = 66;
        $this->showDatePicker = true;
        $this->stepTitle = 'Step 3 - Choose Date';
    }

    public function updatedDate()
    {
        $this->specialistAppointments = Appointment::where('specialist_id', $this->chosenSpecialist)->whereDate('appointment_date', Carbon::parse($this->date)->format('Y-m-d'))->get();
    }

    public function render()
    {
        return view('livewire.create-appointment');
    }

    public function store()
    {
        $this->validate([
            'chosenPatient' => ['required'],
            'chosenSpecialist' => ['required'],
            'date' => ['required'],
            'time' => ['required']
        ]);

        $check = Appointment::where('appointment_date', Carbon::parse($this->date.' '. $this->time)->format('Y-m-d H:i'))->count();
        if($check > 0){
            $this->addError('date','This date is not available.');
            return;
        }

        $appointment = new Appointment();
        $appointment->patient_id = $this->chosenPatient;
        $appointment->user_id = auth()->user()->id;
        $appointment->specialist_id = $this->chosenSpecialist;
        $appointment->appointment_date = Carbon::parse($this->date.' '. $this->time)->format('Y-m-d H:i');
        $appointment->save();
        $this->reset();
        $this->success = true;
    }
}
