<?php

namespace App\Http\Controllers;

use App\Models\Appointment;
use App\Models\Patient;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
         $this->middleware(['auth']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $latest_patient = Patient::orderBy('id', 'desc')->take(5)->get();
        if (auth()->user()->isSpecialist()) {
            $latest_appointment = Appointment::where('specialist_id', auth()->user()->id)->orderBy('appointment_date', 'asc')->take(5)->get();
        } else {
            $latest_appointment = Appointment::orderBy('appointment_date', 'asc')->take(5)->get();
        }
        return view('home', [
            'latest_patients' => $latest_patient,
            'latest_appointments' => $latest_appointment
        ]);

    }

}
