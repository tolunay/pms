<?php

namespace App\Http\Controllers;

use App\Models\Appointment;
use App\Models\Note;
use App\Models\Patient;
use App\Models\Payment;
use App\Models\SpecialistPayment;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AppointmentsController extends Controller
{
    public function index()
    {
        if (auth()->user()->isSpecialist()) {
            $latest = Appointment::where('specialist_id', auth()->user()->id)->orderBy('appointment_date', 'asc')->take(5)->get();
        } else {
            $latest = Appointment::orderBy('appointment_date', 'asc')->take(5)->get();
        }
        return view('appointments.index', [
            'latest_appointments' => $latest
        ]);
    }

    public function create()
    {
        return view('appointments.create');
    }

    public function list()
    {
        return view('appointments.list',[
            'appointments'=> Appointment::all()
        ]);
    }

    public function view(Appointment $appointment)
    {
        if ($appointment->specialist_id == auth()->user()->id && $appointment->seen == false)
        {
            $appointment->seen = true;
            $appointment->seen_at = Carbon::now();
            $appointment->save();
        }
        return view('appointments.view', [
            'appointment' => $appointment,
        ]);
    }

    public function cancel(Appointment $appointment)
    {
        $appointment->status = 2;
        $appointment->save();

        return back()->with('success', 'Appointment canceled successfully.');
    }

    public function complete(Appointment $appointment)
    {
        $appointment->status = 1;
        $appointment->save();

        return back()->with('success', 'Appointment completed successfully.');
    }

    public function pending(Appointment $appointment)
    {
        $appointment->status = 0;
        $appointment->save();

        return back()->with('success', 'Appointment sett to pending successfully.');
    }

    public function addNote(Appointment $appointment, Request $request)
    {
        $request->validate([
            'note' => 'required|min:10'
        ]);

        $note = new Note();
        $note->user_id = auth()->user()->id;
        $note->appointment_id = $appointment->id;
        $note->note = $request->get('note');
        $note->save();

        return back()->with('success', 'Note saved successfully.');
    }

    public function updateAmount(Appointment $appointment, Request $request)
    {
        $request->validate([
            'amount' => 'required|numeric'
        ]);

        $appointment->total_amount = $request->get('amount');

        $appointment->save();

        return back()->with('success', 'Amount updated successfully.');
    }

    public function savePayment(Appointment $appointment, Request $request)
    {
        $request->validate([
            'amount' => 'required|numeric',
            'payment_method' => 'required'
        ]);

        if ($appointment->specialist_id == null) {
            return back()->with('error', 'You have to choose specialist first.');
        }

        $payment = new Payment();
        $payment->user_id = auth()->user()->id;
        $payment->specialist_id = $appointment->specialist_id;
        $payment->appointment_id = $appointment->id;
        $payment->patient_id = $appointment->patient_id;
        $payment->tax_rate = 18;
        $payment->payment_method = $request->get('payment_method');
        $payment->amount = $request->get('amount');
        $payment->save();

        $specialist = User::where('id', $appointment->specialist_id)->first();
        if ($specialist != null) {
            $share = $specialist->rate;
            $amount = $request->get('amount');
            $earning = $amount / 100 * $share;
            $specialist->balance += $earning;
            $specialist->save();

            $specialist_payment = new SpecialistPayment();
            $specialist_payment->specialist_id = $appointment->specialist_id;
            $specialist_payment->appointment_id = $appointment->id;
            $specialist_payment->total_amount = $request->get('amount');
            $specialist_payment->earning = $earning;
            $specialist_payment->rate = $share;
            $specialist_payment->save();
        }

        return back()->with('success', 'Payment saved successfully.');
    }

    public function edit(Appointment $appointment)
    {
        $specialists = User::where('rank', 2)->get();
        return view('appointments.edit', [
            'appointment' => $appointment,
            'specialists' => $specialists,
        ]);
    }

    public function update(Appointment $appointment, Request $request)
    {
        $request->validate([
            'specialist' => ['required'],
            'date' => ['required', 'date'],
        ]);

        $appointment->appointment_date = $request->get('appointment_date');

        $appointment->save();
        return back()->with('success' , 'Appointment successfully was updated.');
    }

    public function destroy(Appointment $appointment)
    {
        if ($appointment->status == 1) {
            return back()->with('error', 'This appointment was marked as completed, therefore you can not delete it.');
        }
        $appointment->delete();
        return redirect(route('appointments'))->with('success', 'Appointment was deleted successfully.');
    }

}
