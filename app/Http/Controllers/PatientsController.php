<?php

namespace App\Http\Controllers;

use App\Models\Appointment;
use App\Models\Patient;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use function GuzzleHttp\Promise\all;

class PatientsController extends Controller
{
    public function index()
    {
        $latest = Patient::orderBy('id', 'desc')->take(5)->get();
        return view('patients.index', [
            'latest_patients' => $latest
        ]);
    }

    public function create()
    {
        return view('patients.create');
    }

    public function login()
    {
        return view('patients.login');
    }

    public function register()
    {
        return view('patients.register');
    }

    public function appointment()
    {
        if (session()->has('patient_email') == false || session()->has('patient_id') == false) {
            abort(503);
        }

        $patient = Patient::where('email', session('patient_email'))->where('id_number', session('patient_id'))->firstOrFail();

        $specialists = User::where('rank', 2)->get();

        return view('patients.appointment', [
            'specialists' => $specialists,
            'patient' => $patient
        ]);
    }

    public function edit(Patient $patient)
    {
        $specialists = User::where('rank', 2)->get();
        return view('patients.edit', [
            'patient' => $patient,
            'specialists' => $specialists,
        ]);
    }

    public function update(Patient $patient, Request $request)
    {
        $request->validate([
            'specialist_id' => ['nullable', 'exists:users,id'],
            'name' => ['required', 'string'],
            'email' => ['required', 'email', 'unique:patients,email,'.$patient->id],
            'phone' => ['required', 'integer', 'unique:patients,phone,'.$patient->id],
            'id_number' => ['required', 'integer', 'unique:patients,id_number,'.$patient->id],
            'birth_date' => ['required', 'date'],
            'w_name' => ['nullable', 'string'],
            'w_birth_date' => ['nullable', 'date'],
            'w_id_number' => ['nullable', 'integer', 'unique:patients,woman_id_number,'.$patient->id],
            'w_occupation' => ['nullable', 'string'],
            'w_phone' => ['nullable', 'integer', 'unique:patients,woman_phone,'.$patient->id],
            'w_email' => ['nullable', 'email', 'unique:patients,woman_email,'.$patient->id],
            'm_name' => ['nullable', 'string'],
            'm_birth_date' => ['nullable', 'date'],
            'm_id_number' => ['nullable', 'integer', 'unique:patients,man_id_number,'.$patient->id],
            'm_occupation' => ['nullable', 'string'],
            'm_phone' => ['nullable', 'integer', 'unique:patients,man_phone,'.$patient->id],
            'm_email' => ['nullable', 'email', 'unique:patients,man_email,'.$patient->id],
        ]);

        $patient->specialist_id = $request->get('specialist_id');
        $patient->name = $request->get('name');
        $patient->email = $request->get('email');
        $patient->phone = $request->get('phone');
        $patient->id_number = $request->get('id_number');
        $patient->type = $request->get('type');
        $patient->birth_date = $request->get('birth_date');
        $patient->woman_name = $request->get('woman_name');
        $patient->woman_birth_date = $request->get('woman_birth_date');
        $patient->woman_id_number = $request->get('woman_id_number');
        $patient->woman_occupation = $request->get('woman_occupation');
        $patient->woman_phone = $request->get('woman_phone');
        $patient->woman_email = $request->get('woman_email');
        $patient->man_name = $request->get('man_name');
        $patient->man_birth_date = $request->get('man_birth_date');
        $patient->man_id_number = $request->get('man_id_number');
        $patient->man_occupation = $request->get('man_occupation');
        $patient->man_phone = $request->get('man_phone');
        $patient->man_email = $request->get('man_email');
        $patient->save();
        return back()->with('success' , 'Patient successfully was updated.');
    }

    public function list($type = null)
    {
        if($type == 'children'){
            $patients = Patient::where('type', 1)->get();
        }elseif($type == 'teenagers'){
            $patients = Patient::where('type', 2)->get();
        }elseif($type == 'adults'){
            $patients = Patient::where('type', 3)->get();
        }elseif($type =='couples'){
            $patients = Patient::where('type', 4)->get();
        }else{
            $patients = Patient::all();
        }

        return view('patients.list', [
            'patients'=> $patients,
            'type'=>$type
        ]);
    }

    public function view(Patient $patient)
    {
        return view('patients.view', [
            'patient' => $patient,
        ]);
    }

    public function loginOrRegister(Request $request)
    {
        if (session()->has('patient_email') && session()->has('patient_id')) {
            return redirect(route('patient.appointment'));
        }
        $request->validate([
            'email' => ['required', 'email'],
            'id_number' => ['required', 'numeric']
        ]);

        $check = Patient::where('email', $request->get('email'))->where('id_number', $request->get('id_number'))->first();

        if ($check == null) {
            return redirect(route('patient.register'));
        }

        session()->put('patient_email', $check->email);
        session()->put('patient_id', $check->id_number);

        return redirect(route('patient.appointment'));
    }

    public function postRegister(Request $request)
    {
        $request->validate([
            'name' => ['required'],
            'email' => ['required', 'email', 'unique:patients'],
            'phone' => ['required', 'numeric', 'unique:patients'],
            'id_number' => ['required', 'numeric', 'unique:patients'],
            'birth_date' => ['required']
        ]);

        $patient = new Patient();
        $patient->name = $request->get('name');
        $patient->email = $request->get('email');
        $patient->phone = $request->get('phone');
        $patient->id_number = $request->get('id_number');
        $patient->type = 3;
        $patient->birth_date = $request->get('birth_date');
        $patient->save();

        session()->put('patient_email', $request->get('email'));
        session()->put('patient_id', $request->get('id_number'));

        return redirect(route('patient.appointment'));

    }

    public function postAppointment(Request $request)
    {
        if (session()->has('patient_email') == false || session()->has('patient_id') == false) {
            abort(404);
        }

        $request->validate([
            'appointment_date' => 'required',
            'appointment_time' => 'required',
            'specialist' => ['required', 'exists:users,id']
        ]);

        $patient = Patient::where('email', session('patient_email'))->where('id_number', session('patient_id'))->firstOrFail();

        $check = Appointment::where('appointment_date', Carbon::parse($request->get('appointment_date').' '. $request->get('appointment_time'))->format('Y-m-d H:i'))->count();
        if($check > 0){
            return back()->with('error', 'This date is not available.');
        }

        $appointment = new Appointment();
        $appointment->patient_id = $patient->id;
        $appointment->user_id = 1;
        $appointment->specialist_id = $request->get('specialist');
        $appointment->appointment_date = Carbon::parse($request->get('appointment_date').' '. $request->get('appointment_time'))->format('Y-m-d H:i');
        $appointment->save();

        session()->put('appointment_id', $appointment->id);

        return back()->with('success', 'Appointment saved successfully.');


    }

    public function logout()
    {
        session()->forget(['patient_email', 'patient_id']);

        return redirect(route('login'));
    }

    public function details()
    {
        if (session()->has('appointment_id') == false) {
            abort(404);
        }
        $appointment = Appointment::where('id', session('appointment_id'))->firstOrFail();
        return view('patients.appointment_details', [
            'appointment' => $appointment
        ]);
    }

    public function destroy(Patient $patient)
    {
        $patient->delete();
        return redirect(route('patients'))->with('success', 'Patient was deleted successfully.');
    }
}
