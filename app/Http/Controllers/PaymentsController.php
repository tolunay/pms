<?php

namespace App\Http\Controllers;

use App\Models\Payment;
use App\Models\SpecialistPayment;
use Illuminate\Http\Request;

class PaymentsController extends Controller
{
    public function index()
    {
        $specialist_payments = SpecialistPayment::orderBy('created_at', 'desc')->get();
        $total_payments = Payment::orderBy('created_at', 'desc')->get();
        return view('payments.index', [
            'specialist_payments' => $specialist_payments,
            'total_payments' => $total_payments
        ]);
    }
}
