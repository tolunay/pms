<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Rules\Rank;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware(['admin']);
    }

    public function index()
    {
        $latest = User::orderBy('id', 'desc')->take(5)->get();
        return view('users.index', [
            'latest_users' => $latest
        ]);
    }

    public function create()
    {
        return view('users.create');
    }

    public function list()
    {
        return view('users.list',[
            'users'=> User::all()
        ]);
    }

    public function edit(User $user)
    {
        return view('users.edit', [
            'user' => $user
        ]);
    }


    public function update(User $user, Request $request)
    {
        $request->validate([
            'name' => ['required', 'string'],
            'email' => ['required', 'email', 'unique:users,email,'.$user->id],
            'phone' => ['required', 'integer', 'unique:users,phone,'.$user->id],
            'password' => ['nullable', 'string', 'min:8'],
            'profession' => ['nullable', 'string'],
            'id_number' => ['required', 'integer', 'unique:users,id_number,'.$user->id],
            'birth_date' => ['required', 'date'],
            'rate' => ['nullable', 'integer','min:0','max:100'],
            'rank' => ['required', new Rank()],
        ]);

        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->phone = $request->get('phone');
        if ($request->get('password') != null) {
            $user->password = Hash::make($request->get('password'));
        }
        $user->profession = $request->get('profession');
        $user->id_number = $request->get('id_number');
        $user->birth_date = $request->get('birth_date');
        $user->rate = $request->get('rate');
        $user->rank = $request->get('rank');
        $user->status = $request->get('status');

        $user->save();
        return back()->with('success' , 'User successfully was updated.');

    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string'],
            'email' => ['required', 'email', 'unique:users'],
            'phone' => ['required', 'integer', 'unique:users'],
            'password' => ['required', 'string', 'min:8'],
            'profession' => ['nullable', 'string'],
            'id_number' => ['required', 'integer', 'unique:users'],
            'birth_date' => ['required', 'date'],
            'rate' => ['nullable', 'integer','min:0','max:100'],
            'rank' => ['required', new Rank()],
        ]);

        $user = new User();
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->phone = $request->get('phone');
        $user->password = Hash::make($request->get('password'));
        $user->profession = $request->get('profession');
        $user->id_number = $request->get('id_number');
        $user->birth_date = $request->get('birth_date');
        $user->rate = $request->get('rate');
        $user->rank = $request->get('rank');

        $user->save();

        return redirect(route('users.create'))->with('success', 'Created a new user successfully.');
    }

    public function view(User $user)
    {
        if (!auth()->user()->isAdmin()) {
            abort(404);
        }
        return view('users.view', [
            'user' => $user,
        ]);
    }
}
