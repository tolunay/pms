<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToPatientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('patients', function (Blueprint $table) {
            $table->string('woman_name')->nullable();
            $table->date('woman_birth_date')->nullable();
            $table->bigInteger('woman_id_number')->nullable()->unique();
            $table->string('woman_occupation')->nullable();
            $table->bigInteger('woman_phone')->nullable()->unique();
            $table->string('woman_email')->nullable();
            $table->string('man_name')->nullable();
            $table->date('man_birth_date')->nullable();
            $table->bigInteger('man_id_number')->nullable()->unique();
            $table->string('man_occupation')->nullable();
            $table->bigInteger('man_phone')->nullable()->unique();
            $table->string('man_email')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('patients', function (Blueprint $table) {
            $table->dropColumn(['woman_name', 'woman_birth_date', 'woman_id_number', 'woman_occupation', 'woman_phone', 'woman_email', 'man_name', 'man_birth_date', 'man_id_number', 'man_occupation', 'man_phone', 'man_email']);
        });
    }
}
