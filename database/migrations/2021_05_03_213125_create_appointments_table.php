<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointments', function (Blueprint $table) {
            $table->id();
            $table->foreignId('patient_id');
            $table->foreignId('user_id');
            $table->timestamp('appointment_date');
            $table->decimal('total_amount', 8,2)->nullable();
            $table->text('description')->nullable();
            $table->smallInteger('status')->default(0);
            /*
             * 0. Pending
             * 1. Completed
             * 2. Canceled
             */
            $table->boolean('confirmed')->default(0);
            $table->timestamp('confirmed_at')->nullable();
            $table->boolean('seen')->default(0);
            $table->timestamp('seen_at')->nullable();
            $table->timestamps();


            $table->foreign('patient_id')
                ->references('id')
                ->on('patients')
                ->onDelete('cascade');
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointments');
    }
}
