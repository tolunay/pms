<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSpecialistPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('specialist_payments', function (Blueprint $table) {
            $table->id();
            $table->foreignId('specialist_id');
            $table->foreignId('appointment_id');
            $table->decimal('total_amount', 8, 2);
            $table->decimal('earning', 8, 2);
            $table->integer('rate');
            $table->timestamps();

            $table->foreign('specialist_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');

            $table->foreign('appointment_id')
                ->references('id')
                ->on('appointments')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('specialist_payments');
    }
}
