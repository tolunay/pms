<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['register' => false]);


Route::middleware(['auth'])->group(function () {
    Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('/appointments', [App\Http\Controllers\AppointmentsController::class, 'index'])->name('appointments');
    Route::get('/appointments/create/', [App\Http\Controllers\AppointmentsController::class, 'create'])->name('appointments.create');
    Route::get('/appointments/list/', [App\Http\Controllers\AppointmentsController::class, 'list'])->name('appointments.list');
    Route::get('/appointments/{appointment}/view', [App\Http\Controllers\AppointmentsController::class, 'view'])->name('appointments.view');
    Route::get('/appointments/{appointment}/edit', [App\Http\Controllers\AppointmentsController::class, 'edit'])->name('appointments.edit');
    Route::put('/appointments/{appointment}/update', [App\Http\Controllers\AppointmentsController::class, 'update'])->name('appointments.update');
    Route::post('/appointments/{appointment}/cancel', [App\Http\Controllers\AppointmentsController::class, 'cancel'])->name('appointments.cancel');
    Route::post('/appointments/{appointment}/complete', [App\Http\Controllers\AppointmentsController::class, 'complete'])->name('appointments.complete');
    Route::post('/appointments/{appointment}/pending', [App\Http\Controllers\AppointmentsController::class, 'pending'])->name('appointments.pending');
    Route::post('/appointments/{appointment}/addnote', [App\Http\Controllers\AppointmentsController::class, 'addNote'])->name('appointments.addnote');
    Route::post('/appointments/{appointment}/updateamount', [App\Http\Controllers\AppointmentsController::class, 'updateAmount'])->name('appointments.updateamount');
    Route::post('/appointments/{appointment}/savepayment', [App\Http\Controllers\AppointmentsController::class, 'savePayment'])->name('appointments.savepayment');
    Route::delete('/appointments/{appointment}/delete', [App\Http\Controllers\AppointmentsController::class, 'destroy'])->name('appointments.delete');

    Route::get('/patients', [App\Http\Controllers\PatientsController::class, 'index'])->name('patients');
    Route::get('/patients/create', [App\Http\Controllers\PatientsController::class, 'create'])->name('patients.create');
    Route::get('/patients/list/{type?}', [App\Http\Controllers\PatientsController::class, 'list'])->name('patients.list');
    Route::get('/patients/{patient}/edit', [App\Http\Controllers\PatientsController::class, 'edit'])->name('patients.edit');
    Route::put('/patients/{patient}/update', [App\Http\Controllers\PatientsController::class, 'update'])->name('patients.update');
    Route::get('/patients/{patient}/view', [App\Http\Controllers\PatientsController::class, 'view'])->name('patients.view');
    Route::delete('/patients/{patient}/delete', [App\Http\Controllers\PatientsController::class, 'destroy'])->name('patients.delete');

    Route::get('/users', [App\Http\Controllers\UsersController::class, 'index'])->name('users');
    Route::post('/users', [App\Http\Controllers\UsersController::class, 'store']);
    Route::get('/users/list', [App\Http\Controllers\UsersController::class, 'list'])->name('users.list');
    Route::get('/users/create', [App\Http\Controllers\UsersController::class, 'create'])->name('users.create');
    Route::get('/users/{user}/edit', [App\Http\Controllers\UsersController::class, 'edit'])->name('users.edit');
    Route::put('/users/{user}/update', [App\Http\Controllers\UsersController::class, 'update'])->name('users.update');
    Route::get('/users/{user}/view', [App\Http\Controllers\UsersController::class, 'view'])->name('users.view');

    Route::get('/payments', [App\Http\Controllers\PaymentsController::class, 'index'])->name('payments');


});

Route::get('/patient/login', [App\Http\Controllers\PatientsController::class, 'login'])->name('patient.login');
Route::post('/patient/loginOrRegister', [App\Http\Controllers\PatientsController::class, 'loginOrRegister'])->name('patient.loginOrRegister');
Route::post('/patient/logout', [App\Http\Controllers\PatientsController::class, 'logout'])->name('patient.logout');
Route::get('/patient/register', [App\Http\Controllers\PatientsController::class, 'register'])->name('patient.register');
Route::post('/patient/register', [App\Http\Controllers\PatientsController::class, 'postRegister']);
Route::get('/patient/appointment', [App\Http\Controllers\PatientsController::class, 'appointment'])->name('patient.appointment');
Route::post('/patient/appointment', [App\Http\Controllers\PatientsController::class, 'postAppointment']);
// Route::get('/test', [App\Http\Controllers\HomeController::class, 'index'])->middleware('specialist')->name('home');


