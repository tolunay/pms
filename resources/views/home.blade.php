@extends('layouts.app')

@section('content')
    <div class="main-content">
        <div class="breadcrumb">
            <h1 class="mr-2">{{ config('app.name') }}</h1>
            <ul>
                <li><a href="">{{ __('Home') }}</a></li>
                <!--<li>Version 2</li>-->
            </ul>
        </div>
        <div class="separator-breadcrumb">
            @admin
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <!-- CARD ICON-->
                        <div class="row">
                            <div class="col-lg-4 col-md-6 col-sm-6">
                                <a href="{{ route('patients.list') }}">
                                    <div class="card card-icon mb-4">
                                        <div class="card-body text-center"><i class="i-Male"></i>
                                            <p class="text-muted mt-2 mb-2">{{ __('Patients') }}</p>
                                            <p class="text-primary text-24 line-height-1 m-0">{{ \App\Models\Patient::count() }}</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6">
                                <a href="{{ route('users.list') }}">
                                    <div class="card card-icon mb-4">
                                        <div class="card-body text-center"><i class="i-Business-Woman"></i>
                                            <p class="text-muted mt-2 mb-2">{{ __('Specialists') }}</p>
                                            <p class="text-primary text-24 line-height-1 m-0">{{ \App\Models\User::where('rank', 2)->count() }}</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6">
                                <a href="{{ route('users.list') }}">
                                    <div class="card card-icon mb-4">
                                        <div class="card-body text-center"><i class="i-Assistant"></i>
                                            <p class="text-muted mt-2 mb-2">{{ __('Assistants') }}</p>
                                            <p class="text-primary text-24 line-height-1 m-0">{{ \App\Models\User::where('rank', 3)->count() }}</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6">
                                <a href="{{ route('appointments.list') }}">
                                    <div class="card card-icon-big alert-success mb-4">
                                        <div class="card-body text-center"><i class="i-Yes"></i>
                                            <p class="text-muted mt-2 mb-2">{{ __('Completed Appointments') }}</p>
                                            <p class="line-height-1 text-title text-18 mt-2 mb-0">{{ \App\Models\Appointment::where('status', 1)->count() }}</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6">
                                <a href="{{ route('appointments.list') }}">
                                    <div class="card card-icon-big alert-warning mb-4">
                                        <div class="card-body text-center"><i class="i-Yes"></i>
                                            <p class="text-muted mt-2 mb-2">{{ __('Pending Appointments') }}</p>
                                            <p class="line-height-1 text-title text-18 mt-2 mb-0">{{ \App\Models\Appointment::where('status', 0)->count() }}</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-6">
                                <a href="{{ route('appointments.list') }}">
                                    <div class="card card-icon-big alert-danger mb-4">
                                        <div class="card-body text-center"><i class="i-Yes"></i>
                                            <p class="text-muted mt-2 mb-2">{{ __('Canceled Appointments') }}</p>
                                            <p class="line-height-1 text-title text-18 mt-2 mb-0">{{ \App\Models\Appointment::where('status', 2)->count() }}</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            @endadmin

        <!-- end of row-->
        <div class="row">
            <div class="col-lg-6 col-md-12">
                <div class="card mb-4">
                    <div class="card-body">
                        <h4 class="card-title mb-3">{{ __('Latest Patients') }}</h4>
                        <p>{{ __('You can see latest 5 patients.') }}</p>
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Phone</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Specialist</th>
                                    <th scope="col">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($latest_patients as $patient)
                                    <tr>
                                        <th scope="row">{{ $loop->iteration }}</th>
                                        <td>{{ $patient->name }}</td>
                                        <td>{{ $patient->phone }}</td>
                                        <td>{{ $patient->email }}</td>
                                        <td>{{ $patient->specialist_id ? $patient->specialist->name : '-' }}</td>
                                        <td>
                                            <a href="{{ route('patients.view', $patient->id) }}" class="btn btn-outline-info mr-2"><i class="nav-icon i-Eye font-weight-bold"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end of col-->
            <div class="col-lg-6 col-md-12">
                <div class="card mb-4">
                    <div class="card-body">
                        <h4 class="card-title mb-3">{{ __('Upcoming Appointments') }}</h4>
                        <p>{{ __('You can see upcoming 5 appointments.') }}</p>
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{ __('Patient') }}</th>
                                    <th>{{ __('Specialist') }}</th>
                                    <th>{{ __('Date') }}</th>
                                    <th>{{ __('Status') }}</th>
                                    <th>{{ __('Action') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($latest_appointments as $appointment)
                                    <tr>
                                        <td>{{ $appointment->id }}</td>
                                        <td>{{ ucwords($appointment->patient->name) }}</td>
                                        <td>{{ ucwords($appointment->specialist->name ) }}</td>
                                        <td>{{ \Carbon\Carbon::parse( $appointment->appointment_date)->format('d/m/Y H:i') }}</td>
                                        <td>{!!  $appointment->getStatus() !!}</td>
                                        <td>
                                            <a href="{{ route('appointments.view', $appointment->id) }}" class="btn btn-outline-info mr-2"><i class="nav-icon i-Eye font-weight-bold"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end of col-->
        </div>
        <!-- end of row-->
        <!-- end of main-content -->
    </div>
@endsection
