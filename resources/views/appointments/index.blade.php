@extends('layouts.app')
@section('title', __('Appointment'))
@section('content')
    <div class="main-content">
        <div class="breadcrumb">
            <h1 class="mr-2">{{ __('Appointments') }}</h1>
            <ul>
                <li><a href="{{ route('home') }}">{{ __('Home') }}</a></li>
                <li>{{ __('Appointments') }}</li>
            </ul>
        </div>
        <div class="separator-breadcrumb border-top"></div>
        <div class="row">
            @include('layouts.alerts')
            <div class="col-lg-6 col-md-12">
                <!-- CARD ICON-->
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="card card-icon mb-4">
                            <a href="{{  route('appointments.create') }}">
                                <div class="card-body text-center"><i class="i-Add"></i>
                                    <p class="text-muted mt-2 mb-2">{{ __('Add A New Appointment') }}</p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="card card-icon mb-4">
                            <a href="{{  route('appointments.list') }}">
                                <div class="card-body text-center"><i class="i-Calendar-4"></i>
                                    <p class="text-muted mt-2 mb-2">{{ __('All Appointments') }}</p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <a href="{{ route('appointments.list') }}">
                            <div class="card card-icon-big alert-success mb-4">
                                <div class="card-body text-center"><i class="i-Yes"></i>
                                    <p class="text-muted mt-2 mb-2">{{ __('Completed Appointments') }}</p>
                                    @if (auth()->user()->isSpecialist())
                                        <p class="line-height-1 text-title text-18 mt-2 mb-0">{{ \App\Models\Appointment::where('status', 1)->where('specialist_id', auth()->user()->id)->count() }}</p>
                                    @else
                                        <p class="line-height-1 text-title text-18 mt-2 mb-0">{{ \App\Models\Appointment::where('status', 1)->count() }}</p>
                                    @endif
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <a href="{{ route('appointments.list') }}">
                            <div class="card card-icon-big alert-warning mb-4">
                                <div class="card-body text-center"><i class="i-Yes"></i>
                                    <p class="text-muted mt-2 mb-2">{{ __('Pending Appointments') }}</p>
                                    @if (auth()->user()->isSpecialist())
                                        <p class="line-height-1 text-title text-18 mt-2 mb-0">{{ \App\Models\Appointment::where('status', 0)->where('specialist_id', auth()->user()->id)->count() }}</p>
                                    @else
                                        <p class="line-height-1 text-title text-18 mt-2 mb-0">{{ \App\Models\Appointment::where('status', 0)->count() }}</p>
                                    @endif
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <a href="{{ route('appointments.list') }}">
                            <div class="card card-icon-big alert-danger mb-4">
                                <div class="card-body text-center"><i class="i-Yes"></i>
                                    <p class="text-muted mt-2 mb-2">{{ __('Canceled Appointments') }}</p>
                                    @if (auth()->user()->isSpecialist())
                                        <p class="line-height-1 text-title text-18 mt-2 mb-0">{{ \App\Models\Appointment::where('status', 2)->where('specialist_id', auth()->user()->id)->count() }}</p>
                                    @else
                                        <p class="line-height-1 text-title text-18 mt-2 mb-0">{{ \App\Models\Appointment::where('status', 2)->count() }}</p>
                                    @endif
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12">
                <div class="card mb-4">
                    <div class="card-body">
                        <h4 class="card-title mb-3">{{ __('Upcoming Appointments') }}</h4>
                        <p>{{ __('You can see upcoming 5 appointments.') }}</p>
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{ __('Patient') }}</th>
                                    <th>{{ __('Date') }}</th>
                                    <th>{{ __('Status') }}</th>
                                    <th>{{ __('Action') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($latest_appointments as $appointment)
                                    <tr>
                                        <td>{{ $appointment->id }}</td>
                                        <td>{{ ucwords($appointment->patient->name) }}</td>
                                        <td>{{ \Carbon\Carbon::parse( $appointment->appointment_date)->format('d/m/Y H:i') }}</td>
                                        <td>{!!  $appointment->getStatus() !!}</td>
                                        <td>
                                            <a href="{{ route('appointments.view', $appointment->id) }}" class="btn btn-outline-primary mr-2"><i class="nav-icon i-Eye font-weight-bold"></i></a>
                                            <a href="{{ route('appointments.edit', $appointment->id) }}" class="btn btn-outline-success mr-2"><i class="nav-icon i-Pen-2 font-weight-bold"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end of row-->
        <!-- end of main-content -->
    </div>
@endsection
