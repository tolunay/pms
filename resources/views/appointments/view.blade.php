@extends('layouts.app')
@section('title', 'Appointment Detail')
@section('header')
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/datatables.min.css') }}" />
@endsection
@section('content')
    <div class="main-content">
        <div class="breadcrumb">
            <h1>{{ __('Appointments') }}</h1>
            <ul>
                <li><a href="{{ route('home') }}">Home</a></li>
                <li><a href="{{ route('appointments') }}">{{ __('Appointments') }}</a></li>
                <li>{{ __('Appointment Detail') }}</li>
            </ul>
        </div>
        <div class="separator-breadcrumb border-top"></div>
        <!-- end of row-->
        <div class="row">
            <div class="col-lg-12 col-md-12">
                @if($errors->any())
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <div>{{ $error }}</div>
                        @endforeach
                    </div>
                @endif
                @if(session()->has('success'))
                    <div class="alert alert-success"> {{ session()->get('success') }}</div>
                @endif
                @if(session()->has('error'))
                    <div class="alert alert-danger"> {{ session()->get('error') }}</div>
                @endif
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title mb-3">{{ __('Appointment Detail') }}</h4>
                        <hr class="mt-0 mb-3">
                        <div class="text-right mb-3">
                            @if($appointment->seen)
                                <span class="text-20 text-success font-weight-light mr-2 mt-2">{{ __('Specialist saw this appointment at '). \Illuminate\Support\Carbon::parse($appointment->seen_at)->format('d/m/Y H:i') }}</span>
                            @else
                                <span class="text-20 text-danger font-weight-light mr-2 mt-2">{{ __('Specialist did not see this appointment yet.') }}</span>
                            @endif

                            <?php if(!auth()->user()->isAssistant()): ?>
                            <a href="javascript:void(0)" class="btn btn-primary" onclick="event.preventDefault();
                                    document.getElementById('complete-form').submit();"><i class="i-Yes"></i> Complete</a>

                            <form id="complete-form" action="{{ route('appointments.complete', $appointment->id) }}" method="POST" class="d-none">
                                @csrf
                            </form>
                            @if ($appointment->status != 0)
                                <a href="javascript:void(0)" class="btn btn-warning" onclick="event.preventDefault();
                                    document.getElementById('pending-form').submit();"><i class="i-Timer1"></i> Pending</a>

                                <form id="pending-form" action="{{ route('appointments.pending', $appointment->id) }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            @endif
                            @if($appointment->status != 2)
                                <a href="javascript:void(0)" class="btn btn-danger" onclick="event.preventDefault();
                                    document.getElementById('cancel-form').submit();"><i class="i-Remove"></i> Cancel</a>

                                <form id="cancel-form" action="{{ route('appointments.cancel', $appointment->id) }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            @endif
                        </div>
                        <?php endif;?>
                        <div class="row">

                            <div class="col-md-3">
                                <div class="card card-icon mb-4">
                                    <a href="{{ route('patients.edit', $appointment->patient_id) }}">
                                        <div class="card-body text-center"><i class="i-Administrator"></i>
                                            <p class="text-muted mt-2 mb-2">{{ __('Name') }}</p>
                                            <p class="lead text-22 m-0">{{ ucwords($appointment->patient->name) }}</p>
                                        </div>
                                    </a>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="card card-icon mb-4">
                                    <div class="card-body text-center"><i class="i-Calendar-4"></i>
                                        <p class="text-muted mt-2 mb-2">{{ __('Appointment Date') }}</p>
                                        <p class="lead text-22 m-0">{{ \Carbon\Carbon::parse( $appointment->appointment_date)->format('d/m/Y H:i') }}</p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="card card-icon mb-4">
                                    <div class="card-body text-center"><i class="i-Clock"></i>
                                        <p class="text-muted mt-2 mb-2">{{ __('Status') }}</p>
                                        <p class="lead text-22 m-0">{!!  $appointment->getStatus() !!}</p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="card card-icon mb-4">
                                    <div class="card-body text-center"><i class="i-Business-Man"></i>
                                        <p class="text-muted mt-2 mb-2">{{ __('Specialist') }}</p>
                                        <p class="lead text-22 m-0">{{ ucwords($appointment->specialist->name ) }}</p>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="row mt-4">
                            <div class="col-lg-12 col-md-12 mb-4">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title mb-3">{{ __('Payment Details') }}</h4>
                                        <hr class="mt-0 mb-3">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="card card-icon-big alert-warning mb-4">
                                                    <div class="card-body text-center"><i class="i-Money-Bag"></i>
                                                        <p class="text-muted mt-2 mb-2">{{ __('Appointment Price') }}</p>
                                                        <p class="line-height-1 text-title text-18 mt-2 mb-0">{{ $appointment->total_amount ? $appointment->total_amount : '-'}}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="card card-icon-big alert-success mb-4">
                                                    <div class="card-body text-center"><i class="i-Money"></i>
                                                        <p class="text-muted mt-2 mb-2">{{ __('Total Paid') }}</p>
                                                        <p class="line-height-1 text-title text-18 mt-2 mb-0">{{ \App\Models\Payment::where('appointment_id', $appointment->id)->sum('amount') }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="card card-icon-big alert-danger mb-4">
                                                    <div class="card-body text-center"><i class="i-Money-2"></i>
                                                        <p class="text-muted mt-2 mb-2">{{ __('Remaning Balance') }}</p>
                                                        <p class="line-height-1 text-title text-18 mt-2 mb-0">
                                                            @if($appointment->total_amount)
                                                                {{ $appointment->total_amount - \App\Models\Payment::where('appointment_id', $appointment->id)->sum('amount') }}
                                                            @else
                                                                -
                                                            @endif
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <form action="{{ route('appointments.updateamount', $appointment->id) }}" method="POST">
                                            @csrf
                                            <div class="col-md-6">
                                                <div class="form-group mb-3 p-0">
                                                    <label>{{ __('Appointment Price') }}</label>
                                                    <input type="number" class="form-control" name="amount" value="{{ old('amount') ? old('amount') : $appointment->total_amount }}">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <button class="btn btn-primary btn-rounded" type="submit"> {{ __('Update') }} </button>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-md-6">
                                        <form action="{{ route('appointments.savepayment', $appointment->id) }}" method="POST">
                                            @csrf
                                            <div class="col-md-6">
                                                <div class="form-group mb-3 p-0">
                                                    <label>{{ __('Payment Method') }}</label>
                                                    <select name="payment_method" class="form-control">
                                                        <option value="">Choose</option>
                                                        <option value="Cash">Cash</option>
                                                        <option value="Credit Card">Credit Card</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group mb-3 p-0">
                                                    <label>{{ __('Total Paid') }}</label>
                                                    <input type="number" class="form-control" name="amount" value="">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <button class="btn btn-primary btn-rounded" type="submit"> {{ __('Save') }} </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <div class="row">
                            <div class="col-md-12">
                                <div class="card mt-4">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <h4 class="card-title mb-3">{{ __('Notes') }}</h4>
                                                <hr class="mt-0 mb-3">
                                                @forelse($appointment->notes as $note)
                                                    <p><strong>{{ \Carbon\Carbon::parse( $note->created_at)->format('d/m/Y H:i')  }}</strong></p>
                                                    <p>{{ $note->note }}</p>
                                                @empty

                                                @endforelse
                                            </div>

                                            <div class="col-md-6">
                                                <h4 class="card-title mb-3">{{ __('Add Note') }}</h4>
                                                <hr class="mt-0 mb-3">
                                                <div class="card card-icon mb-4">
                                                    <a href="#" data-toggle="modal" data-target="#exampleModal">
                                                        <div class="card-body text-center"><i class="i-Add"></i>
                                                            <p class="lead text-22 m-0">{{ __('Add Notes') }}</p>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--  Modal -->
                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <form action="{{ route('appointments.addnote',$appointment->id ) }}" method="POST">
                                        @csrf
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Add Note</h5>
                                            <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                                        </div>
                                        <div class="modal-body">
                                            <textarea name="note" id="" cols="50" rows="10" class="form-control"></textarea>
                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn btn-secondary" type="button" data-dismiss="modal">Close</button>
                                            <button class="btn btn-primary ml-2" type="submit">Save</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- end of main-content -->
    </div>
@endsection
@section('footer')
    <script src="{{ asset('assets/js/plugins/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/js/scripts/datatables.script.min.js') }}"></script>
@endsection
