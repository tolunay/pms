@extends('layouts.app')
@section('title', 'Edit Appointment')
@section('content')
    <div class="main-content">
        <div class="breadcrumb">
            <h1 class="mr-2">{{ __('Appointments') }}</h1>
            <ul>
                <li><a href="{{ route('home') }}">{{ __('Home') }}</a></li>
                <li><a href="{{ route('appointments.list') }}">{{ __('Appointments') }}</a></li>
                <li>{{ __('Edit Appointment') }}</li>
            </ul>
        </div>
        <div class="separator-breadcrumb border-top"></div>
        <div class="row">
            <div class="col-lg-12 col-md-12">
                @if($errors->any())
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <div>{{ $error }}</div>
                        @endforeach
                    </div>
                @endif
                @if(session()->has('success'))
                    <div class="alert alert-success"> {{ session()->get('success') }}</div>
                @endif
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title mb-3">Edit Appointments</h4>
                        <hr class="mt-0">
                        <form action="{{ route('appointments.update', $appointment->id) }}" method="post">
                            @csrf
                            @method('PUT')
                            <div class="mt-2">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group mb-3 r1 r2 r3">
                                                    <label>Specialist</label>
                                                    <select class="form-control form-control-rounded" name="specialist">
                                                        <option value="">{{ __('Please Select A Specialist') }}</option>
                                                        @foreach($specialists as $specialist)
                                                            <option value="{{ $specialist->id }}" @if($specialist->id == $appointment->specialist_id) selected @endif>{{ $specialist->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group mb-3">
                                                    <label>Date</label>
                                                    <input class="form-control form-control-rounded" type="date" placeholder="Date" name="date" value="{{ old('date') ? old('date') : \Illuminate\Support\Carbon::parse($appointment->appointment_date)->format('Y-m-d') }}">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group mb-3">
                                                    <label>Time</label>
                                                    <input class="form-control form-control-rounded" type="time" placeholder="Time" name="time" value="{{ old('time')  ? old('time') : \Illuminate\Support\Carbon::parse($appointment->appointment_date)->format('H:i') }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <button type="submit" class="btn btn-primary text-white btn-rounded">Update</button>
                            <button type="button" class="btn btn-danger text-white btn-rounded" onclick="confirm_delete()" >Delete</button>

                        </form>
                        <form id="delete-form" action="{{ route('appointments.delete', $appointment->id) }}" method="POST" class="d-none">
                            @csrf
                            @method('DELETE')
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- end of row-->
        <!-- end of main-content -->
    </div>
@endsection

@section('footer')
    <script type="text/javascript">
        function confirm_delete() {
            var result = confirm("Are you sure you want to delete this appointment ? This action can not be undone!");
            if (result) {
                event.preventDefault();
                document.getElementById('delete-form').submit();
            }
        }
    </script>
@endsection
