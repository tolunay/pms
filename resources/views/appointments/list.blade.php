@extends('layouts.app')
@section('title', 'Appointments List')
@section('header')
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/datatables.min.css') }}" />
@endsection
@section('content')
    <div class="main-content">
        <div class="breadcrumb">
            <h1>{{ __('Appointments') }}</h1>
            <ul>
                <li><a href="{{ route('home') }}">Home</a></li>
                <li><a href="{{ route('appointments') }}">{{ __('Appointments') }}</a></li>
                <li>
                    {{ __('All Appointments List') }}
                </li>
            </ul>
        </div>
        <div class="separator-breadcrumb border-top"></div>
        <!-- end of row-->
        <div class="row mb-4">
            <div class="col-md-12 mb-4">
                <div class="card text-left">
                    <div class="card-body">
                        <h4 class="card-title mb-3">
                                {{ __('All Appointments List') }}
                        </h4>
                        <div class="table-responsive">
                            <table class="display table table-striped table-bordered" id="deafult_ordering_table" style="width:100%">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{ __('Patient') }}</th>
                                    <th>{{ __('Specialist') }}</th>
                                    <th>{{ __('Date') }}</th>
                                    <th>{{ __('Status') }}</th>
                                    <th>{{ __('Created at') }}</th>
                                    <th>{{ __('Action') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($appointments as $appointment)
                                    <tr>
                                        <td>{{ $appointment->id }}</td>
                                        <td>{{ ucwords($appointment->patient->name) }}</td>
                                        <td>{{ ucwords($appointment->specialist->name ) }}</td>
                                        <td>{{ \Carbon\Carbon::parse( $appointment->appointment_date)->format('d/m/Y H:i') }}</td>
                                        <td>{!!  $appointment->getStatus() !!}</td>
                                        <td>{{  \Carbon\Carbon::parse( $appointment->created_at)->format('d/m/Y H:i')  }}</td>
                                        <td>
                                            <a href="{{ route('appointments.view', $appointment->id) }}" class="btn btn-outline-info mr-2"><i class="nav-icon i-Eye font-weight-bold"></i></a>
                                            <a href="{{ route('appointments.edit', $appointment->id) }}" class="btn btn-outline-success mr-2"><i class="nav-icon i-Pen-2 font-weight-bold"></i></a>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end of col-->
        </div>
        <!-- end of row-->
        <!-- end of main-content -->
    </div>
@endsection
@section('footer')
    <script src="{{ asset('assets/js/plugins/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/js/scripts/datatables.script.min.js') }}"></script>
@endsection
