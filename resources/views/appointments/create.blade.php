@extends('layouts.app')
@section('title', 'Add A New Appointment')
@section('content')

    <!-- ============ Body content start ============= -->
    <div class="main-content">
        <div class="breadcrumb">
            <h1>{{ __('Appointments') }}</h1>
            <ul>
                <li><a href="{{ route('home') }}">Home</a></li>
                <li><a href="{{ route('appointments') }}">{{ __('Appointments') }}</a></li>
                <li>{{ __('Create A New Appointment') }}</li>
            </ul>
        </div>
        <div class="separator-breadcrumb border-top"></div>
        <livewire:create-appointment />
    </div>

    <style>

        .search-bar-in {
            display: block;
            align-items: center;
            justify-content: left;
            background: #f8f9fa;
            border: 1px solid #eee;
            border-radius: 20px;
            position: relative;
            width: 50%;
            height: 40px;
            margin: 0 auto;
            margin-bottom: 30px;
        }

        .search-bar-in input {
            background: transparent;
            border: 0;
            color: #212121;
            font-size: .8rem;
            line-height: 2;
            height: 100%;
            outline: initial !important;
            padding: .5rem 1rem;
            width: calc(100% - 32px);
        }
    </style>
@endsection
