@extends('layouts.app')
@section('title', 'Add A New Patient')
@section('content')
    <div class="main-content">
        <div class="breadcrumb">
            <h1>{{ __('Patients') }}</h1>
            <ul>
                <li><a href="{{ route('home') }}">Home</a></li>
                <li><a href="{{ route('patients') }}">{{ __('Patients') }}</a></li>
                <li>{{ __('Create A New Patient') }}</li>
            </ul>
        </div>
        <div class="separator-breadcrumb border-top"></div>
        <livewire:add-patient />
    </div>
@endsection
