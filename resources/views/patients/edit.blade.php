@extends('layouts.app')
@section('title', 'Edit Patient')
@section('content')
    <div class="main-content">
        <div class="breadcrumb">
            <h1 class="mr-2">{{ __('Patients') }}</h1>
            <ul>
                <li><a href="{{ route('home') }}">{{ __('Home') }}</a></li>
                <li><a href="{{ route('patients.list') }}">{{ __('Patients') }}</a></li>
                <li>{{ __('Edit Patient') }}</li>
            </ul>
        </div>
        <div class="separator-breadcrumb border-top"></div>
        <div class="row">
            <div class="col-lg-12 col-md-12">
                @include('layouts.alerts')
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title mb-3">Edit Patient</h4>
                        <hr class="mt-0">
                        <form action="{{ route('patients.update', $patient->id) }}" method="post">
                            @csrf
                            @method('PUT')
                            <div class="mt-2">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group mb-3 r1 r2 r3">
                                                    <label>Full Name</label>
                                                    <input class="form-control form-control-rounded" type="text" placeholder="Full Name" name="name" value="{{ old('name') ? old('name') : $patient->name }}">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group mb-3 r1 r2 r3">
                                                    <label>Email</label>
                                                    <input class="form-control form-control-rounded" type="text" placeholder="Email" name="email" value="{{ old('email') ? old('email') : $patient->email }}">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group mb-3 r1 r2 r3">
                                                    <label>Phone</label>
                                                    <input class="form-control form-control-rounded" type="text" placeholder="Phone" name="phone" value="{{ old('phone') ? old('phone') : $patient->phone }}">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group mb-3 r1 r2 r3">
                                                    <label>ID Number</label>
                                                    <input class="form-control form-control-rounded" type="text" placeholder="ID Number" name="id_number" value="{{ old('id_number') ? old('id_number') : $patient->id_number }}">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group mb-3 r1 r2 r3">
                                                    <label>Birth Date</label>
                                                    <input class="form-control form-control-rounded" type="date" placeholder="Birth Date" name="birth_date" value="{{ old('birth_date') ? old('birth_date') : $patient->birth_date }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if($patient->type == 4)
                            <div class="couples">
                                <div class="row">
                                    <!-- Woman -->
                                    <div class="col-md-6">
                                        <div class="card-title mb-0 mt-2">Woman</div>
                                        <hr class="my-2">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group mb-3 p-0">
                                                    <label>Full Name</label>
                                                    <input class="form-control form-control-rounded" type="text" placeholder="Full Name" name="w_name"  value="{{ old('w_name') ? old('w_name') : $patient->woman_name }}">
                                                </div>
                                                <div class="form-group mb-3 p-0">
                                                    <label>Birth Date</label>
                                                    <input class="form-control form-control-rounded" type="text" placeholder="Birth Date" name="w_birth_date"  value="{{ old('w_birth_date') ? old('w_birth_date') : $patient->woman_birth_date }}">
                                                </div>
                                                <div class="form-group mb-3 p-0">
                                                    <label>ID Number</label>
                                                    <input class="form-control form-control-rounded" type="text" placeholder="ID Number" name="w_id_number"  value="{{ old('w_id_number') ? old('w_id_number') : $patient->woman_id_number }}">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group mb-3 p-0">
                                                    <label>Occupation</label>
                                                    <input class="form-control form-control-rounded" type="text" placeholder="Occupation" name="w_occupation"  value="{{ old('w_occupation') ? old('w_occupation') : $patient->woman_occupation }}">
                                                </div>
                                                <div class="form-group mb-3 p-0">
                                                    <label>Phone</label>
                                                    <input class="form-control form-control-rounded" type="text" placeholder="Phone" name="w_phone"  value="{{ old('w_phone') ? old('w_phone') : $patient->woman_phone }}">
                                                </div>
                                                <div class="form-group mb-3 p-0">
                                                    <label>Email</label>
                                                    <input class="form-control form-control-rounded" type="text" placeholder="Email" name="w_email"  value="{{ old('w_email') ? old('w_email') : $patient->woman_email }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Woman -->
                                    <div class="col-md-6">
                                        <div class="card-title mb-0 mt-2">Man</div>
                                        <hr class="my-2">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group mb-3 p-0">
                                                    <label>Full Name</label>
                                                    <input class="form-control form-control-rounded" type="text" placeholder="Full Name" name="m_name" value="{{ old('m_email') ? old('m_email') : $patient->man_email }}">
                                                </div>
                                                <div class="form-group mb-3 p-0">
                                                    <label>Birth Date</label>
                                                    <input class="form-control form-control-rounded" type="text" placeholder="Birth Date" name="m_birth_date" value="{{ old('m_email') ? old('m_email') : $patient->man_email }}">
                                                </div>
                                                <div class="form-group mb-3 p-0">
                                                    <label>ID Number</label>
                                                    <input class="form-control form-control-rounded" type="text" placeholder="ID Number" name="m_id_number" value="{{ old('m_email') ? old('m_email') : $patient->man_email }}">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group mb-3 p-0">
                                                    <label>Occupation</label>
                                                    <input class="form-control form-control-rounded" type="text" placeholder="Occupation" name="m_occupation" value="{{ old('m_email') ? old('m_email') : $patient->man_email }}">
                                                </div>
                                                <div class="form-group mb-3 p-0">
                                                    <label>Phone</label>
                                                    <input class="form-control form-control-rounded" type="text" placeholder="Phone" name="m_phone" value="{{ old('m_email') ? old('m_email') : $patient->man_email }}">
                                                </div>
                                                <div class="form-group mb-3 p-0">
                                                    <label>Email</label>
                                                    <input class="form-control form-control-rounded" type="text" placeholder="Email" name="m_email" value="{{ old('m_email') ? old('m_email') : $patient->man_email }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group mb-3 p-0">
                                        <label>Please Select A Specialist</label>
                                        <select class="form-control form-control-rounded" name="specialist">
                                            <option>Please Select A Specialist</option>
                                            @foreach($specialists as $specialist)
                                            <option value="{{ $specialist->id }}" @if($specialist->id == $patient->specialist_id) selected @endif>{{ $specialist->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>


                            <button type="submit" class="btn btn-primary text-white btn-rounded">Update</button>
                            <button type="button" class="btn btn-danger text-white btn-rounded" onclick="confirm_delete()">Delete</button>

                        </form>

                        <form id="delete-form" action="{{ route('patients.delete', $patient->id) }}" method="POST" class="d-none">
                            @csrf
                            @method('DELETE')
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- end of row-->
        <!-- end of main-content -->
    </div>
@endsection

@section('footer')
    <script type="text/javascript">
        function confirm_delete() {
            var result = confirm("Are you sure you want to delete this appointment ? This action can not be undone!");
            if (result) {
                event.preventDefault();
                document.getElementById('delete-form').submit();
            }
        }
    </script>
@endsection
