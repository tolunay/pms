<!DOCTYPE html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ config('app.name', 'Laravel') }} - {{ __('Register') }}</title>
    <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,400i,600,700,800,900" rel="stylesheet">
    <link href="{{ asset('assets/css/themes/lite-purple.min.css') }}" rel="stylesheet">
</head>
<div class="auth-layout-wrap" style="background-image: url({{ asset('assets/images/photo-wide-4.jpg') }})">
    <div class="auth-content">
        <div class="card o-hidden">
            <div class="row">
                <div class="col-md-12">
                    @if($errors->any())
                        <div class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <div>{{ $error }}</div>
                            @endforeach
                        </div>
                    @endif
                    @if(session()->has('success'))
                        <div class="alert alert-success"> {{ session('success') }}</div>
                    @endif
                        @if(session()->has('error'))
                            <div class="alert alert-danger"> {{ session('error') }}</div>
                        @endif
                    <div class="p-4">
                        <h1 class="mb-3 text-18">Welcome back, {{ $patient->name }}</h1>
                        <form method="POST" action="{{ route('patient.appointment') }}">
                            @csrf
                            <div class="form-group">
                                <label>Please Select A Specialist</label>
                                <select class="form-control form-control-rounded" name="specialist">
                                    <option value="" readonly="">Please Select A Specialist</option>
                                    @foreach($specialists as $specialist)
                                        <option value="{{ $specialist->id }}">{{ ucfirst($specialist->name) }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="email">Appointment Date</label>
                                <div class="row">
                                <div class="col-md-6"><input class="form-control form-control-rounded" id="appointment_date" type="date" name="appointment_date"></div>
                                <div class="col-md-6"><input type="time" min="09:00" max="18:00" step="1800" class="form-control" name="appointment_time"></div>
                                </div>
                            </div>
                            <button class="btn btn-primary btn-rounded mt-3" type="submit">{{ __('Save') }}</button>
                            <button class="btn btn-danger btn-rounded mt-3"  onclick="event.preventDefault();
                                    document.getElementById('logout-patient-form').submit();">{{ __('Cancel') }}</button>
                        </form>

                        <form id="logout-patient-form" action="{{ route('patient.logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

