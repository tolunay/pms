@extends('layouts.app')
@section('title', 'Patients List')
@section('header')
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/datatables.min.css') }}" />
@endsection
@section('content')
    <div class="main-content">
            <div class="breadcrumb">
                <h1>{{ __('Patients') }}</h1>
                <ul>
                    <li><a href="{{ route('home') }}">Home</a></li>
                    <li><a href="{{ route('patients') }}">{{ __('Patients') }}</a></li>
                    <li>
                        @if($type == 'children')
                            {{ __('Child Patients List') }}
                            @elseif($type == 'teenagers')
                            {{ __('Teenager Patients List') }}
                        @elseif($type == 'adults')
                            {{ __('Adult Patients List') }}
                        @elseif($type == 'couples')
                            {{ __('Couple Patients List') }}
                        @else
                            {{ __('All Patients List') }}
                        @endif
                    </li>
                </ul>
            </div>
            <div class="separator-breadcrumb border-top"></div>
            <!-- end of row-->
            <div class="row mb-4">
                <div class="col-md-12 mb-4">
                    <div class="card text-left">
                        <div class="card-body">
                            <h4 class="card-title mb-3">
                                @if($type == 'children')
                                    {{ __('Child Patients List') }}
                                @elseif($type == 'teenagers')
                                    {{ __('Teenager Patients List') }}
                                @elseif($type == 'adults')
                                    {{ __('Adult Patients List') }}
                                @elseif($type == 'couples')
                                    {{ __('Couple Patients List') }}
                                @else
                                    {{ __('All Patients List') }}
                                @endif
                            </h4>
                            <div class="btn-group mb-2" role="group">
                                <a href="{{ route('patients.list') }}"><button class="btn btn-secondary mr-2" type="button">{{ __('All') }}</button></a>
                                <a href="{{ route('patients.list', ['type'=>'children']) }}"><button class="btn btn-secondary mr-2" type="button">{{ __('Children') }}</button></a>
                                <a href="{{ route('patients.list', ['type'=>'teenagers']) }}"><button class="btn btn-secondary mr-2" type="button">{{ __('Teenagers') }}</button></a>
                                <a href="{{ route('patients.list', ['type'=>'adults']) }}"><button class="btn btn-secondary mr-2" type="button">{{ __('Adults') }}</button></a>
                                <a href="{{ route('patients.list', ['type'=>'couples']) }}"><button class="btn btn-secondary" type="button">{{ __('Couples') }}</button></a>
                            </div>
                            <div class="table-responsive">
                                <table class="display table table-striped table-bordered" id="deafult_ordering_table" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{ __('Name') }}</th>
                                        <th>{{ __('Phone') }}</th>
                                        <th>{{ __('Email') }}</th>
                                        <th>{{ __('Type') }}</th>
                                        <th>{{ __('Specialist') }}</th>
                                        <th>{{ __('Created at') }}</th>
                                        <th>{{ __('Action') }}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($patients as $patient)
                                    <tr>
                                        <td>{{ $patient->id }}</td>
                                        <td>{{ $patient->name }}</td>
                                        <td>{{ $patient->phone }}</td>
                                        <td>{{ $patient->email }}</td>
                                        <td>{{ $patient->getType() }}</td>
                                        <td>{{ $patient->specialist_id ? $patient->specialist->name : '-' }}</td>
                                        <td>{{ $patient->created_at }}</td>
                                        <td>
                                            <a href="{{ route('patients.view',$patient->id) }}" class="btn btn-outline-primary mr-2"><i class="nav-icon i-Eye font-weight-bold"></i></a>
                                            <a href="{{ route('patients.edit',$patient->id) }}" class="btn btn-outline-success mr-2"><i class="nav-icon i-Pen-2 font-weight-bold"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end of col-->
            </div>
            <!-- end of row-->
            <!-- end of main-content -->
        </div>
@endsection
@section('footer')
    <script src="{{ asset('assets/js/plugins/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/js/scripts/datatables.script.min.js') }}"></script>
@endsection
