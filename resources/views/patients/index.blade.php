@extends('layouts.app')
@section('title', 'Patients')
@section('content')
    <div class="main-content">
        <div class="breadcrumb">
            <h1 class="mr-2">{{ __('Patients') }}</h1>
            <ul>
                <li><a href="{{ route('home') }}">{{ __('Home') }}</a></li>
                <li>{{ __('Patients') }}</li>
            </ul>
        </div>
        <div class="separator-breadcrumb border-top"></div>
        <div class="row">
            @include('layouts.alerts')
            <div class="col-lg-6 col-md-12">
                <!-- CARD ICON-->
                <div class="row">
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <a href="{{  route('patients.create') }}">
                            <div class="card card-icon mb-4">
                                <div class="card-body text-center"><i class="i-Add"></i>
                                    <p class="text-muted mt-2 mb-2">{{ __('Add A New Patient') }}</p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="card card-icon mb-4">
                            <a href="{{  route('patients.list') }}">
                                <div class="card-body text-center"><i class="i-Calendar-4"></i>
                                    <p class="text-muted mt-2 mb-2">{{ __('All Patients') }}</p>
                                    <p class="line-height-1 text-title text-18 mt-2 mb-0">{{ \App\Models\Patient::count() }}</p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="card card-icon mb-4">
                            <a href="{{ route('patients.list', ['type'=> 'children']) }}">
                                <div class="card-body text-center"><i class="i-Baby"></i>
                                    <p class="text-muted mt-2 mb-2">{{ __('Child Patients') }}</p>
                                    <p class="line-height-1 text-title text-18 mt-2 mb-0">{{ \App\Models\Patient::where('type', 1)->count() }}</p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="card card-icon-big mb-4">
                            <a href="{{ route('patients.list', ['type'=> 'teenagers']) }}">
                                <div class="card-body text-center"><i class="i-Yes"></i>
                                    <p class="text-muted mt-2 mb-2">{{ __('Teenager Patients') }}</p>
                                    <p class="line-height-1 text-title text-18 mt-2 mb-0">{{ \App\Models\Patient::where('type', 2)->count() }}</p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="card card-icon-big mb-4">
                            <a href="{{ route('patients.list', ['type'=> 'adults']) }}">
                                <div class="card-body text-center"><i class="i-Yes"></i>
                                    <p class="text-muted mt-2 mb-2">{{ __('Adult Patients') }}</p>
                                    <p class="line-height-1 text-title text-18 mt-2 mb-0">{{ \App\Models\Patient::where('type', 3)->count() }}</p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="card card-icon-big mb-4">
                            <a href="{{ route('patients.list', ['type'=> 'couples']) }}">
                            <div class="card-body text-center"><i class="i-Yes"></i>
                                    <p class="text-muted mt-2 mb-2">{{ __('Couples Patients') }}</p>
                                    <p class="line-height-1 text-title text-18 mt-2 mb-0">{{ \App\Models\Patient::where('type', 4)->count() }}</p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12">
                <div class="card mb-4">
                    <div class="card-body">
                        <h4 class="card-title mb-3">{{ __('Latest Patients') }}</h4>
                        <p>{{ __('You can see latest 5 patients.') }}</p>
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Phone</th>
                                    <th scope="col">Specialist</th>
                                    <th scope="col">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($latest_patients as $patient)
                                    <tr>
                                        <th scope="row">{{ $loop->iteration }}</th>
                                        <td>{{ $patient->name }}</td>
                                        <td>{{ $patient->phone }}</td>
                                        <td>{{ $patient->specialist_id ? $patient->specialist->name : '-' }}</td>
                                        <td>
                                            <a href="{{ route('patients.view',$patient->id) }}" class="btn btn-outline-primary mr-2"><i class="nav-icon i-Eye font-weight-bold"></i></a>
                                            <a href="{{ route('patients.edit',$patient->id) }}" class="btn btn-outline-success mr-2"><i class="nav-icon i-Pen-2 font-weight-bold"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end of row-->
        <!-- end of main-content -->
    </div>
@endsection
