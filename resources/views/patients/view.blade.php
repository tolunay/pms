@extends('layouts.app')
@section('title', 'Patient Detail')
@section('header')
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/datatables.min.css') }}" />
@endsection
@section('content')
    <div class="main-content">
        <div class="breadcrumb">
            <h1>{{ __('Patients') }}</h1>
            <ul>
                <li><a href="{{ route('home') }}">Home</a></li>
                <li><a href="{{ route('patients') }}">{{ __('Patients') }}</a></li>
                <li>{{ __('Patient Detail') }}</li>
            </ul>
        </div>
        <div class="separator-breadcrumb border-top"></div>
        <!-- end of row-->
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title mb-3">{{ __('Patient Detail') }}</h4>
                        <hr class="mt-0 mb-3">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 mb-4">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title mb-3">{{ __('Personal Information') }}</h4>
                                        <hr class="mt-0 mb-3">
                                        <div class="user-profile mb-4">
                                            <div class="ul-widget-card__user-info border-bottom-gray-200 pb-3">
                                                <p class="text-24">{{ $patient->name }}</p><span class="badge badge-pill badge-primary p-2 m-1" data-toggle="tooltip" title="" data-placement="top" data-original-title="Kayıt Tarihi">{{ $patient->created_at }}</span>
                                                <div class="mt-3 ul-widget-app__skill-margin text-left">
                                                    <span class="d-block"><strong>ID Number: </strong>{{ $patient->id_number }}</span>
                                                    <span class="d-block"><strong>Phone: </strong>{{ $patient->phone }}</span>
                                                    <span class="d-block"><strong>E-Mail:</strong>{{ $patient->email }}</span>
                                                    <span class="d-block"><strong>Type: </strong>{{ $patient->getType() }}</span>
                                                    <span class="d-block"><strong>Specialist: </strong>{{ $patient->specialist_id ? $patient->specialist->name : '-' }}</span>
                                                </div>

                                            </div>
                                            <div class="ul-widget-app__profile-footer mt-4">
                                                <div class="ul-widget-app__profile-footer-font">
                                                    <a href="{{ route('patients.edit',$patient->id) }}" class="btn btn-outline-success mr-2"><i class="nav-icon i-Pen-2 font-weight-bold"></i> Edit</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-8 col-md-8 mb-4">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title mb-3">{{ __('Payment Information') }}</h4>
                                        <hr class="mt-0 mb-3">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="card card-icon-big alert-warning mb-4">
                                                    <div class="card-body text-center"><i class="i-Money-Bag"></i>
                                                        <p class="text-muted mt-2 mb-2">Sub Total</p>
                                                        <p class="line-height-1 text-title text-18 mt-2 mb-0">{{ config('app.money_prefix') }}{{ \App\Models\Appointment::where('patient_id', $patient->id)->sum('total_amount') }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="card card-icon-big alert-success mb-4">
                                                    <div class="card-body text-center"><i class="i-Money"></i>
                                                        <p class="text-muted mt-2 mb-2">Paid</p>
                                                        <p class="line-height-1 text-title text-18 mt-2 mb-0">{{ config('app.money_prefix') }}{{ \App\Models\Payment::where('patient_id', $patient->id)->sum('amount') }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="card card-icon-big alert-danger mb-4">
                                                    <div class="card-body text-center"><i class="i-Money-2"></i>
                                                        <p class="text-muted mt-2 mb-2">Remaining</p>
                                                        <p class="line-height-1 text-title text-18 mt-2 mb-0">{{ config('app.money_prefix') }}{{ \App\Models\Appointment::where('patient_id', $patient->id)->sum('total_amount') - \App\Models\Payment::where('patient_id', $patient->id)->sum('amount') }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card mt-4">
                                    <div class="card-body">
                                        <h4 class="card-title mb-1">{{ __('Appointments') }}</h4>
                                    </div>
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Specialist</th>
                                            <th scope="col">Date</th>
                                            <th scope="col">Status</th>
                                            <th scope="col">Created At</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @forelse($patient->appointments as $appointment)
                                        <tr>
                                            <td>{{ $appointment->id }}</td>
                                            <td>{{ $appointment->specialist->name }}</td>
                                            <td>{{ \Carbon\Carbon::parse( $appointment->appointment_date)->format('d/m/Y H:i') }}</td>
                                            <td>{!! $appointment->getStatus() !!}</td>
                                            <td>{{ \Carbon\Carbon::parse( $appointment->created_at)->format('d/m/Y H:i') }}</td>
                                            <td>
                                                <a href="{{ route('appointments.view', $appointment->id) }}" class="btn btn-outline-primary mr-2"><i class="nav-icon i-Eye font-weight-bold"></i></a>
                                                <a href="{{ route('appointments.edit', $appointment->id) }}" class="btn btn-outline-success mr-2"><i class="nav-icon i-Pen-2 font-weight-bold"></i></a>
                                            </td>
                                        </tr>
                                        @empty
                                            <tr>
                                                <td>No appointment yet.</td>
                                            </tr>
                                        @endforelse
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end of main-content -->
    </div>
@endsection
@section('footer')
    <script src="{{ asset('assets/js/plugins/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/js/scripts/datatables.script.min.js') }}"></script>
@endsection
