<!DOCTYPE html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ config('app.name', 'Laravel') }} - {{ __('Register') }}</title>
    <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,400i,600,700,800,900" rel="stylesheet">
    <link href="{{ asset('assets/css/themes/lite-purple.min.css') }}" rel="stylesheet">
</head>
<div class="auth-layout-wrap" style="background-image: url({{ asset('assets/images/photo-wide-4.jpg') }})">
    <div class="auth-content">
        <div class="card o-hidden">
            <div class="row">
                <div class="col-md-6 text-center" style="background-size: cover;background-image: url({{ asset('assets/images/photo-long-3.jpg') }})">
                    <div class="pl-3 auth-right">
                        <div class="auth-logo text-center mt-4"><img src="{{ asset('assets/images/logo.png') }}" alt=""></div>
                        <div class="flex-grow-1"></div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="p-4">
                        <h1 class="mb-3 text-18">Login / Signup</h1>
                        <form method="POST" action="{{ route('patient.loginOrRegister') }}">
                            @csrf
                            <div class="form-group">
                                <label for="email">Email address</label>
                                <input class="form-control form-control-rounded" id="email" type="email" name="email">
                            </div>
                            <div class="form-group">
                                <label for="email">ID Number</label>
                                <input class="form-control form-control-rounded" id="id_number" type="text" name="id_number">
                            </div>
                            <button class="btn btn-primary btn-block btn-rounded mt-3">{{ __('Log in / Signup') }}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
