<!DOCTYPE html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ config('app.name', 'Laravel') }} - {{ __('Login') }}</title>
    <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,400i,600,700,800,900" rel="stylesheet">
    <link href="{{ asset('assets/css/themes/lite-purple.min.css') }}" rel="stylesheet">
</head>
<div class="auth-layout-wrap" style="background-image: url(assets/images/photo-wide-4.jpg)">
    <div class="auth-content">
        <div class="card o-hidden">
            <div class="row">
                <div class="col-md-6">
                    <div class="p-4">
                        <div class="auth-logo text-center mb-4"><img src="{{ asset('assets/images/logo.png') }}" alt=""></div>
                        <h1 class="mb-3 text-18">{{ __('Login') }}</h1>
                        <form method="POST" action="{{ route('login') }}">
                            @csrf
                            <div class="form-group">
                                <label for="email">{{ __('E-mail') }}</label>
                                <input class="form-control form-control-rounded @error('email') is-invalid @enderror" id="email" type="email" name="email">
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="password">{{ __('Password') }}</label>
                                <input class="form-control form-control-rounded @error('password') is-invalid @enderror" id="password" type="password" name="password">
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                        <label class="form-check-label" for="remember">
                                            {{ __('Remember Me') }}
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <button class="btn btn-rounded btn-primary btn-block mt-2">{{ __('Sign In') }}</button>
                        </form>
                        <div class="mt-3 text-center"><a class="text-muted" href="{{ route('password.request') }}">
                                <u>{{ __('Forgot Your Password?') }}</u></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 text-center" style="background-size: cover;background-image: url({{ asset('assets/images/photo-long-3.jpg') }})">
                    <div class="pr-3 auth-right">
                        <a class="btn btn-rounded btn-warning btn-email btn-block btn-icon-text" href="{{ route('patient.login') }}"><i class="i-Mail-with-At-Sign"></i> {{ __('Click here get appointment!') }}</a>
{{--                        <a class="btn btn-rounded btn-outline-primary btn-outline-email btn-block btn-icon-text" href="{{ route('register') }}"><i class="i-Mail-with-At-Sign"></i> {{ __('Sign up with Email') }}</a>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
