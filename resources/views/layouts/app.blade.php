<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width,initial-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    @hasSection('title')
        <title>{{ config('app.name', 'Laravel') }} - @yield('title') </title>
    @else
        <title>{{ config('app.name', 'Laravel') }}</title>
    @endif
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,400i,600,700,800,900" rel="stylesheet" />
    <link href="{{ asset('assets/css/themes/lite-purple.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/plugins/perfect-scrollbar.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('custom.css') }}" rel="stylesheet" />
    @yield('header')
    @livewireStyles
</head>

<body class="text-left">
<div class="app-admin-wrap layout-sidebar-large">
    <x-main-header />
    <x-left-sidebar />
    <!-- =============== Left side End ================-->
    <div class="main-content-wrap sidenav-open d-flex flex-column">
        <!-- ============ Body content start ============= -->
        @yield('content')
        @include('layouts.footer')
    </div>
</div>
<script src="{{ asset('assets/js/plugins/jquery-3.3.1.min.js')}}"></script>
<script src="{{ asset('assets/js/plugins/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/perfect-scrollbar.min.js') }}"></script>
<script src="{{ asset('assets/js/scripts/script.min.js') }}"></script>
<script src="{{ asset('assets/js/scripts/sidebar.large.script.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/echarts.min.js') }}"></script>
<script src="{{ asset('assets/js/scripts/echart.options.min.js') }}"></script>
<script src="{{ asset('assets/js/plugins/datatables.min.js') }}"></script>
<script src="{{ asset('assets/js/scripts/dashboard.v2.script.min.js') }}"></script>
<script src="{{ asset('assets/js/scripts/customizer.script.min.js') }}"></script>
@yield('footer')
@livewireScripts
</body>

</html>
