<!-- Footer Start -->
<div class="flex-grow-1"></div>
<div class="app-footer">
    <div class="row">
        <div class="col-md-9">
            <p><strong>{{ config('app.name') }}</strong></p>
            <p>
                It is a psychology appointment system where you create, list appointments and check payments.
            </p>
        </div>
    </div>
    <div class="footer-bottom border-top pt-3 d-flex flex-column flex-sm-row align-items-center">
        <a class="btn btn-primary text-white btn-rounded" href="#" target="_blank">Contact Us</a>
        <span class="flex-grow-1"></span>
        <div class="d-flex align-items-center">
            <img class="logo" src="{{ asset('assets/images/logo.png') }}" alt="">
            <div>
                <p class="m-0">&copy; 2021</p>
                <p class="m-0">All rights reserved</p>
            </div>
        </div>
    </div>
</div>
<!-- fotter end -->
