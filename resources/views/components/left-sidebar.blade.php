<div>
    <div class="side-content-wrap">
        <div class="sidebar-left open rtl-ps-none" data-perfect-scrollbar="" data-suppress-scroll-x="true">
            <ul class="navigation-left">
                <li class="nav-item"><a class="nav-item-hold" href="{{ route('home') }}"><i
                            class="nav-icon i-Bar-Chart"></i><span class="nav-text">Home</span></a>
                    <div class="triangle"></div>
                </li>
                <li class="nav-item"><a class="nav-item-hold" href="{{ route('appointments') }}"><i
                            class="nav-icon i-Calendar-4"></i><span class="nav-text">Appointments</span></a>
                    <div class="triangle"></div>
                </li>
                <li class="nav-item"><a class="nav-item-hold" href="{{ route('patients') }}"><i
                            class="nav-icon i-MaleFemale"></i><span class="nav-text">Patients</span></a>
                    <div class="triangle"></div>
                </li>
                @admin
                <li class="nav-item"><a class="nav-item-hold" href="{{ route('users') }}"><i
                            class="nav-icon i-Administrator"></i><span class="nav-text">Users</span></a>
                    <div class="triangle"></div>
                </li>
                @endadmin
{{--                <li class="nav-item"><a class="nav-item-hold" href="#"><i--}}
{{--                            class="nav-icon i-Data-Settings"></i><span class="nav-text">Settings</span></a>--}}
{{--                    <div class="triangle"></div>--}}
{{--                </li>--}}
                <li class="nav-item"><a class="nav-item-hold" href="{{ route('logout') }}" onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();"><i
                            class="nav-icon i-Power-2"></i><span class="nav-text">Logout</span></a>
                    <div class="triangle"></div>
                </li>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                    @csrf
                </form>
            </ul>
        </div>
        <div class="sidebar-overlay"></div>
    </div>
</div>
