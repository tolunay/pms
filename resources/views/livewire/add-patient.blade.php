<div>
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <div class="card-body">
                    @if($errors->any())
                        <div class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <div>{{ $error }}</div>
                            @endforeach
                        </div>
                    @endif
                    @if($success)
                            <div class="alert alert-success"> {{ __('New patient was successfully saved.') }}</div>
                    @endif
                    <h4 class="card-title mb-3">{{ __('Create A New Patient') }}</h4>
                    <hr class="mt-0">
                    <form action="" method="post">
                        <div class="btn-group" >
                            <label class="btn btn-default ">
                                <input type="radio" name="patient" value="1" wire:model="patient"> {{ __('Child') }}
                            </label>
                            <label class="btn btn-default">
                                <input type="radio" name="patient" value="2" wire:model="patient"> {{ __('Teenager') }}
                            </label>
                            <label class="btn btn-default">
                                <input type="radio" name="patient" value="3" wire:model="patient"> {{ __('Adult') }}
                            </label>
                            <label class="btn btn-default">
                                <input type="radio" name="patient" value="4" wire:model="patient"> {{ __('Couples') }}
                            </label>
                        </div>
                        <div class="mt-2">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group mb-3 r1 r2 r3">
                                                <label>{{ __('Full Name') }}</label>
                                                <input class="form-control form-control-rounded" type="text" placeholder="{{ __('Full Name') }}" name="name" wire:model.lazy="name">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group mb-3 r1 r2 r3">
                                                <label>{{ __('Email') }}</label>
                                                <input class="form-control form-control-rounded" type="text" placeholder="{{ __('Email') }}" name="email" wire:model.lazy="email">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group mb-3 r1 r2 r3">
                                                <label>{{ __('Phone') }}</label>
                                                <input class="form-control form-control-rounded" type="text" placeholder="{{ __('Phone') }}" name="phone" wire:model.lazy="phone">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group mb-3 r1 r2 r3">
                                                <label>{{ __('ID Number') }}</label>
                                                <input class="form-control form-control-rounded" type="text" placeholder="{{ __('ID Number') }}" name="id_number" wire:model.lazy="id_number">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group mb-3 r1 r2 r3">
                                                <label>{{ __('Birth Date') }}</label>
                                                <input class="form-control form-control-rounded" type="date" placeholder="{{ __('Birth Date') }}" name="birth_date" wire:model.lazy="birth_date">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="couples" style="@if($patient != 4) display: none; @endif">
                            <div class="row">
                                <!-- Woman -->
                                <div class="col-md-6">
                                    <div class="card-title mb-0 mt-2">{{ __('Woman') }}</div>
                                    <hr class="my-2">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group mb-3 p-0">
                                                <label>{{ __('Full Name') }}</label>
                                                <input class="form-control form-control-rounded" type="text" placeholder="{{ __('Full Name') }}" name="w_name" wire:model.lazy="w_name">
                                            </div>
                                            <div class="form-group mb-3 p-0">
                                                <label>{{ __('Birth Date') }}</label>
                                                <input class="form-control form-control-rounded" type="text" placeholder="{{ __('Birth Date') }}" name="w_birth_date" wire:model.lazy="w_birth_date">
                                            </div>
                                            <div class="form-group mb-3 p-0">
                                                <label>{{ __('ID Number') }}</label>
                                                <input class="form-control form-control-rounded" type="text" placeholder="{{ __('ID Number') }}" name="w_id_number" wire:model.lazy="w_id_number">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group mb-3 p-0">
                                                <label>{{ __('Occupation') }}</label>
                                                <input class="form-control form-control-rounded" type="text" placeholder="{{ __('Occupation') }}" name="w_occupation" wire:model.lazy="w_occupation">
                                            </div>
                                            <div class="form-group mb-3 p-0">
                                                <label>{{ __('Phone') }}</label>
                                                <input class="form-control form-control-rounded" type="text" placeholder="{{ __('Phone') }}" name="w_phone" wire:model.lazy="w_phone">
                                            </div>
                                            <div class="form-group mb-3 p-0">
                                                <label>{{ __('Email') }}</label>
                                                <input class="form-control form-control-rounded" type="text" placeholder="{{ __('Email') }}" name="w_email" wire:model.lazy="w_email">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Woman -->
                                <div class="col-md-6">
                                    <div class="card-title mb-0 mt-2">{{ __('Man') }}</div>
                                    <hr class="my-2">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group mb-3 p-0">
                                                <label>{{ __('Full Name') }}</label>
                                                <input class="form-control form-control-rounded" type="text" placeholder="{{ __('Full Name') }}" name="m_name" wire:model.lazy="m_name">
                                            </div>
                                            <div class="form-group mb-3 p-0">
                                                <label>{{ __('Birth Date') }}</label>
                                                <input class="form-control form-control-rounded" type="text" placeholder="{{ __('Birth Date') }}" name="m_birth_date" wire:model.lazy="m_birth_date">
                                            </div>
                                            <div class="form-group mb-3 p-0">
                                                <label>{{ __('ID Number') }}</label>
                                                <input class="form-control form-control-rounded" type="text" placeholder="{{ __('ID Number') }}" name="m_id_number" wire:model.lazy="m_id_number">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group mb-3 p-0">
                                                <label>{{ __('Occupation') }}</label>
                                                <input class="form-control form-control-rounded" type="text" placeholder="{{ __('Occupation') }}" name="m_occupation" wire:model.lazy="m_occupation">
                                            </div>
                                            <div class="form-group mb-3 p-0">
                                                <label>{{ __('Phone') }}</label>
                                                <input class="form-control form-control-rounded" type="text" placeholder="{{ __('Phone') }}" name="m_phone" wire:model.lazy="m_phone">
                                            </div>
                                            <div class="form-group mb-3 p-0">
                                                <label>{{ __('Email') }}</label>
                                                <input class="form-control form-control-rounded" type="text" placeholder="{{ __('Email') }}" name="m_email" wire:model.lazy="m_email">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group mb-3 p-0">
                                    <label>{{ __('Please Select A Specialist') }}</label>
                                    <select class="form-control form-control-rounded" name="specialist" wire:model.lazy="specialist_id" >
                                        <option>{{ __('Please Select A Specialist') }}</option>
                                        @foreach($specialists as $specialist)
                                         <option value="{{ $specialist->id }}">{{ $specialist->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>


                        <button type="button" class="btn btn-primary text-white btn-rounded" wire:click="store">{{ __('Add') }}</button>

                    </form>
                </div>
            </div>
        </div>
    </div><!-- end of main-content -->
</div>
