<div>

    @section('header')
        <link rel="stylesheet" href="{{ asset('vendor/bootstrap-datepicker-1.9.0-dist/css/bootstrap-datepicker.css') }}">
    @endsection
    <div class="row">
        <div class="col-lg-12 col-md-12">
            @if($errors->any())
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <div>{{ $error }}</div>
                    @endforeach
                </div>
            @endif
            @if($success)
                <div class="alert alert-success"> {{ __('New appointment was successfully created.') }}</div>
            @endif
                <div class="col-md-12">
                    <h2>
                        <div class="float-left"> {{ $stepTitle }}</div>
                        <div class="float-right">
                            <a href="{{ route('patients.create') }}" class="btn btn-info"><i class="i-Add"></i> Create New Patient</a>
                        </div>
                        <div class="clearfix"></div>
                    </h2>
                    <div class="progress mt-4">
                        <div class="progress-bar progress-bar-warning progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: {{ $progress }}%">

                        </div>
                    </div>
                    <hr />
                </div>

                <!-- Step 1 -->
                <div class="step-1">
                    <div class="col-md-12">
                        <div class="search-bar-in">
                            <input type="text" autocomplete="off" wire:model.lazy="search" placeholder="Search Patient (ID Number, Name, Phone or Email)">
                            <i class="search-icon text-muted i-Magnifi-Glass1"></i>
                        </div>



                        @if($showPatients )
                            <div class="results">
                                <div class="row">


                                    <div class="col-md-4">
                                        <a href="{{ route('patients.create') }}" class="select-user alert-success">
                                            <div class="row">
                                                <div class="col-md-3 ">
                                                    <div class="icon-bg">
                                                        <i class="i-Add"></i>
                                                    </div>
                                                </div>
                                                <div class="col-md-9">
                                                    <div class="user-text">{{ __('Create A New Patient') }}</div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>



                                    @forelse($patients as $patient)
                                    <div class="col-md-4">
                                        <a class="select-user alert-primary">
                                            <div class="row">
                                                <div class="col-md-3 ">
                                                    <div class="icon-bg">
                                                        <i class="i-Add-User"></i>
                                                    </div>
                                                </div>
                                                <div class="col-md-9">
                                                    <div class="user-text">{{ $patient->name }}</div>
                                                    <div class="btn btn-primary text-white btn-rounded select" wire:click="choosePatient({{ $patient->id }})">Choose</div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>

                                    @empty
                                        <div class="col-md-4">
                                            <a class="select-user alert-danger">
                                                <div class="row">
                                                    <div class="col-md-3 ">
                                                        <div class="icon-bg">
                                                            <i class="i-Close"></i>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-9">
                                                        <div class="user-text">No patient was found.</div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    @endforelse
                                </div>
                            </div>
                        @endif
                    </div>
                </div>

            @if($showSpecialists)
            <!-- Step 2 -->
            <div class="step-2">
                <div class="col-md-12">
                    <div class="results">
                        <div class="row">

                            @forelse($specialists as $specialist)
                                <div class="col-md-4">
                                    <a class="select-user alert-primary">
                                        <div class="row">
                                            <div class="col-md-3 ">
                                                <div class="icon-bg">
                                                    <i class="i-Add-User"></i>
                                                </div>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="user-text">{{ $specialist->name }}</div>
                                                <div class="btn btn-primary text-white btn-rounded select" wire:click="chooseSpecialist({{ $specialist->id }})">Choose</div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                            @empty

                                <div class="col-md-4">
                                    <a class="select-user alert-danger">
                                        <div class="row">
                                            <div class="col-md-3 ">
                                                <div class="icon-bg">
                                                    <i class="i-Close"></i>
                                                </div>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="user-text">No specialist was found.</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            @endforelse
                        </div>
                    </div>
                </div>
            </div>
            @endif

        @if($showDatePicker)
            <!-- Step 3 -->
                <div class="step-3">
                    <div class="col-md-12">
                        <div class="results">
                            <div class="row">
                                <div class="col-md-6">
                                    <div wire:ignore class="mb-4">
                                        <input type="date" wire:model.lazy="date" class="form-control">
                                    </div>
                                    <div wire:ignore>
                                        <input type="time" min="09:00" max="18:00" step="1800" wire:model.lazy="time" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    @if($specialistAppointments)
                                        @if(!$specialistAppointments->isEmpty())

                                            <table class="table">
                                                <thead class="thead-light">
                                                <tr>
                                                    <th scope="col">{{ __('Patient') }}</th>
                                                    <th scope="col">{{ __('Time') }}</th>
                                                    <th scope="col">{{ __('Status') }}</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($specialistAppointments as $appointment)
                                                    <tr>
                                                        <td>{{ $appointment->patient->name }}</td>
                                                        <td>{{ \Carbon\Carbon::parse($appointment->appointment_date)->format('H:i')}}</td>
                                                        <td class="font-weight-bold text-success">{{ $appointment->getStatusText() }}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                         @else
                                            <p>No appointments found for this date.</p>
                                            @endif
                                        @endif
                                </div>
                                @if($date && $time)
                                <div class="col-md-12">
                                    <button type="button" class="btn btn-primary text-white btn-rounded float-right" wire:click="store">Create</button>
                                </div>
                                    @endif

                            </div>
                        </div>
                    </div>
                </div>
            @endif


        </div>
    </div><!-- end of main-content -->
    @section('footer')
        <script src="{{ asset('vendor/bootstrap-datepicker-1.9.0-dist/js/bootstrap-datepicker.js') }}"></script>
        <script>
            $('#datepicker input').datepicker({
                weekStart: 1,
                format: "dd.mm.yyyy",
                language: "tr",
                daysOfWeekDisabled: "0",
                daysOfWeekHighlighted: "0",
                autoclose: true,
                todayHighlight: true,
                datesDisabled: [],
                toggleActive: true
            });
        </script>
    @endsection
</div>
