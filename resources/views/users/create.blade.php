@extends('layouts.app')
@section('title', 'Add A New User')
@section('content')
    <div class="main-content">
        <div class="breadcrumb">
            <h1>{{ __('Users') }}</h1>
            <ul>
                <li><a href="{{ route('home') }}">Home</a></li>
                <li><a href="{{ route('users') }}">{{ __('Users') }}</a></li>
                <li>{{ __('Create A New User') }}</li>
            </ul>
        </div>
        <div class="separator-breadcrumb border-top"></div>
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="card-body">
                        @if($errors->any())
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <div>{{ $error }}</div>
                                @endforeach
                            </div>
                        @endif
                        @if(session()->has('success'))
                            <div class="alert alert-success"> {{ session('success') }}</div>
                        @endif
                        <h4 class="card-title mb-3">{{ __('Create A New User') }}</h4>
                        <hr class="mt-0 mb-3">
                        <form action="{{ route('users') }}" method="post">
                            @csrf
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group mb-3 p-0">
                                                <label>{{ __('Full Name') }}</label>
                                                <input class="form-control form-control-rounded" type="text" value="{{ old('name') }}" placeholder="{{ __('Please enter full name') }}" name="name">
                                            </div>
                                            <div class="form-group mb-3 p-0">
                                                <label>{{ __('Email') }}</label>
                                                <input class="form-control form-control-rounded" type="text" value="{{ old('email') }}" placeholder="{{ __('Please enter email') }}" name="email">
                                            </div>
                                            <div class="form-group mb-3 p-0">
                                                <label>{{ __('ID Number') }}</label>
                                                <input class="form-control form-control-rounded" type="text" value="{{ old('id_number') }}" placeholder="{{ __('Please enter ID Number') }}" name="id_number">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group mb-3 p-0">
                                                <label>{{ __('Phone') }}</label>
                                                <input class="form-control form-control-rounded" type="text" value="{{ old('phone') }}" placeholder="{{ __('Please enter phone') }}" name="phone">
                                            </div>
                                            <div class="form-group mb-3 p-0">
                                                <label>{{ __('Birth Date') }}</label>
                                                <input class="form-control form-control-rounded" type="date" value="{{ old('birth_date') }}" placeholder="{{ __('Please enter birth date') }}" name="birth_date">
                                            </div>
                                            <div class="form-group mb-3 p-0">
                                                <label>{{ __('Profession') }}</label>
                                                <input class="form-control form-control-rounded" type="text" value="{{ old('profession') }}" placeholder="{{ __('Please enter profession') }}" name="profession">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group mb-3 p-0">
                                                <label>{{ __('Profit Share Rate %') }}</label>
                                                <input class="form-control form-control-rounded" type="text" value="{{ old('rate') }}" placeholder="{{ __('Please enter profit share rate (0-100)') }}" name="rate">
                                            </div>
                                            <div class="form-group mb-3 p-0">
                                                <label>{{ __('Password') }}</label>
                                                <input class="form-control form-control-rounded" type="password" placeholder="{{ __('Please enter password') }}" name="password">
                                            </div>
                                            <div class="form-group mb-3 p-0">
                                                <label>{{ __('Please select rank') }}</label>
                                                <select class="form-control form-control-rounded" name="rank">
                                                    <option value="1">Admin</option>
                                                    <option value="2">Specialist</option>
                                                    <option value="3">Assistant</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-primary text-white btn-rounded">Add</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
