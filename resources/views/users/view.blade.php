@extends('layouts.app')
@section('title', 'User Detail')
@section('header')
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/datatables.min.css') }}" />
@endsection
@section('content')
    <div class="main-content">
        <div class="breadcrumb">
            <h1>{{ __('Users') }}</h1>
            <ul>
                <li><a href="{{ route('home') }}">Home</a></li>
                <li><a href="{{ route('users') }}">{{ __('Users') }}</a></li>
                <li>{{ __('User Detail') }}</li>
            </ul>
        </div>
        <div class="separator-breadcrumb border-top"></div>
        <!-- end of row-->
        <div class="row">
            <div class="col-md-3">
                <div class="card card-icon alert-primary mb-4">
                    <div class="card-body text-center"><i class="i-Data-Upload"></i>
                        <p class="mt-2 mb-2">{{ __('Appointments') }}</p>
                        <p class="lead text-22 m-0">{{ \App\Models\Appointment::where('specialist_id', $user->id)->count() }}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card card-icon alert-success mb-4">
                    <div class="card-body text-center"><i class="i-Add-User"></i>
                        <p class="mt-2 mb-2">{{ __('Completed Appointments') }}</p>
                        <p class="lead text-22 m-0">{{ \App\Models\Appointment::where('specialist_id', $user->id)->where('status',1)->count() }}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card card-icon alert-warning mb-4">
                    <div class="card-body text-center"><i class="i-Money-2"></i>
                        <p class="mt-2 mb-2">{{ __('Pending Appointments') }}</p>
                        <p class="lead text-22 m-0">{{ \App\Models\Appointment::where('specialist_id', $user->id)->where('status',0)->count() }}</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card card-icon alert-danger mb-4">
                    <div class="card-body text-center"><i class="i-Money-2"></i>
                        <p class="mt-2 mb-2">{{ __('Canceled Appointments') }}</p>
                        <p class="lead text-22 m-0">{{ \App\Models\Appointment::where('specialist_id', $user->id)->where('status',2)->count() }}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="card card-icon mb-4">
                    <div class="card-body text-center"><i class="i-Money-2"></i>
                        <p class="text-muted mt-2 mb-2">{{ __('Total Income') }}</p>
                        <p class="lead text-22 m-0">{{ $user->payments->sum('earning') }}</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- end of main-content -->
    </div>
@endsection
@section('footer')
    <script src="{{ asset('assets/js/plugins/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/js/scripts/datatables.script.min.js') }}"></script>
@endsection
