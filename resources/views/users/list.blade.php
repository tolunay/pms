@extends('layouts.app')
@section('title', 'Users List')
@section('header')
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/datatables.min.css') }}" />
@endsection
@section('content')
    <div class="main-content">
        <div class="breadcrumb">
            <h1>{{ __('Users') }}</h1>
            <ul>
                <li><a href="{{ route('home') }}">Home</a></li>
                <li><a href="{{ route('users') }}">{{ __('Users') }}</a></li>
                <li>{{ __('All Users List') }}</li>
            </ul>
        </div>
        <div class="separator-breadcrumb border-top"></div>
        <!-- end of row-->
        <div class="row mb-4">
            <div class="col-md-12 mb-4">
                <div class="card text-left">
                    <div class="card-body">
                        <h4 class="card-title mb-3">{{ __('All Users List') }}</h4>
                        <div class="table-responsive">
                            <table class="display table table-striped table-bordered" id="deafult_ordering_table" style="width:100%">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{ __('Name') }}</th>
                                    <th>{{ __('Phone') }}</th>
                                    <th>{{ __('Email') }}</th>
                                    <th>{{ __('Profession') }}</th>
                                    <th>{{ __('Rank') }}</th>
                                    <th>{{ __('Status') }}</th>
                                    <th>{{ __('Created at') }}</th>
                                    <th>{{ __('Action') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <td>{{ $user->id }}</td>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->phone ? $user->phone : '-' }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>{{ $user->profession ? $user->profession : '-' }}</td>
                                        <td>{{ $user->getRank() }}</td>
                                        <td>{{ $user->getStatus() }}</td>
                                        <td>{{ \Carbon\Carbon::parse($user->created_at)->format('d/m/Y H:i') }}</td>
                                        <td>
                                            @if ($user->isSpecialist())
                                                <a href="{{ route('users.view',$user->id) }}" class="btn btn-outline-primary mr-2"><i class="nav-icon i-Eye font-weight-bold"></i></a>
                                            @endif
                                            <a href="{{ route('users.edit',$user->id) }}" class="btn btn-outline-success mr-2"><i class="nav-icon i-Pen-2 font-weight-bold"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end of col-->
        </div>
        <!-- end of row-->
        <!-- end of main-content -->
    </div>
@endsection
@section('footer')
    <script src="{{ asset('assets/js/plugins/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/js/scripts/datatables.script.min.js') }}"></script>
@endsection
