@extends('layouts.app')
@section('title', 'Users')
@section('content')
    <div class="main-content">
        <div class="breadcrumb">
            <h1 class="mr-2">{{ __('Users') }}</h1>
            <ul>
                <li><a href="{{ route('home') }}">{{ __('Home') }}</a></li>
                <li>{{ __('Users') }}</li>
            </ul>
        </div>
        <div class="separator-breadcrumb border-top"></div>
        <div class="row">
            <div class="col-lg-6 col-md-12">
                <!-- CARD ICON-->
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <a href="{{  route('users.create') }}">
                            <div class="card card-icon mb-4">
                                <div class="card-body text-center"><i class="i-Add"></i>
                                    <p class="text-muted mt-2 mb-2">{{ __('Add A New User') }}</p>
                                    <p class="line-height-1 text-title text-18 mt-2 mb-0">	&nbsp;</p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="card card-icon mb-4">
                            <a href="{{  route('users.list') }}">
                                <div class="card-body text-center"><i class="i-Business-ManWoman"></i>
                                    <p class="text-muted mt-2 mb-2">{{ __('All Users') }}</p>
                                    <p class="line-height-1 text-title text-18 mt-2 mb-0">{{ \App\Models\User::count() }}</p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <a href="{{  route('users.list') }}">
                            <div class="card card-icon-big mb-4">
                                <div class="card-body text-center"><i class="i-Assistant"></i>
                                    <p class="text-muted mt-2 mb-2">{{ __('Assistants') }}</p>
                                    <p class="line-height-1 text-title text-18 mt-2 mb-0">{{ \App\Models\User::where('rank',3)->count() }}</p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <a href="{{  route('users.list') }}">
                            <div class="card card-icon-big mb-4">
                                <div class="card-body text-center"><i class="i-Business-Woman"></i>
                                    <p class="text-muted mt-2 mb-2">{{ __('Specialists') }}</p>
                                    <p class="line-height-1 text-title text-18 mt-2 mb-0">{{ \App\Models\User::where('rank',2)->count() }}</p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <a href="{{  route('users.list') }}">
                            <div class="card card-icon-big mb-4">
                                <div class="card-body text-center"><i class="i-Administrator"></i>
                                    <p class="text-muted mt-2 mb-2">{{ __('Administrators') }}</p>
                                    <p class="line-height-1 text-title text-18 mt-2 mb-0">{{ \App\Models\User::where('rank',1)->count() }}</p>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12">
                <div class="card mb-4">
                    <div class="card-body">
                        <h4 class="card-title mb-3">{{ __('Latest Users') }}</h4>
                        <p>{{ __('You can see latest 5 users.') }}</p>
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Phone</th>
                                    <th scope="col">Rank</th>
                                    <th scope="col">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($latest_users as $user)
                                    <tr>
                                        <th scope="row">{{ $loop->iteration }}</th>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->phone ? $user->phone : '-' }}</td>
                                        <td>{{ $user->getRank() }}</td>
                                        <td>
                                            @if ($user->isSpecialist())
                                                <a href="{{ route('users.view',$user->id) }}" class="btn btn-outline-primary mr-2"><i class="nav-icon i-Eye font-weight-bold"></i></a>
                                            @endif
                                            <a href="{{ route('users.edit',$user->id) }}" class="btn btn-outline-success mr-2"><i class="nav-icon i-Pen-2 font-weight-bold"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end of row-->
        <!-- end of main-content -->
    </div>
@endsection
