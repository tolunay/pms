@extends('layouts.app')
@section('title', 'Edit User')
@section('content')
    <div class="main-content">
        <div class="breadcrumb">
            <h1 class="mr-2">{{ __('Users') }}</h1>
            <ul>
                <li><a href="{{ route('home') }}">{{ __('Home') }}</a></li>
                <li><a href="{{ route('users.list') }}">{{ __('Users') }}</a></li>
                <li>{{ __('Edit User') }}</li>
            </ul>
        </div>
        <div class="separator-breadcrumb border-top"></div>
        <div class="row">
            <div class="col-md-12">
                @if($errors->any())
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <div>{{ $error }}</div>
                        @endforeach
                    </div>
                @endif
                @if(session()->has('success'))
                    <div class="alert alert-success"> {{ session()->get('success') }}</div>
                @endif
                <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-3">Edit User</h4>
                    <hr class="mt-0 mb-3">
                    <form action="{{ route('users.update', $user->id) }}" method="post">
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group mb-3 p-0">
                                            <label>Full Name</label>
                                            <input class="form-control form-control-rounded" type="text" value="{{ old('name') ? old('name') : $user->name }}" placeholder="Please enter full name" name="name">
                                        </div>
                                        <div class="form-group mb-3 p-0">
                                            <label>Email</label>
                                            <input class="form-control form-control-rounded" type="text" value="{{ old('email') ? old('email') : $user->email }}" placeholder="Please enter email" name="email">
                                        </div>
                                        <div class="form-group mb-3 p-0">
                                            <label>ID Number</label>
                                            <input class="form-control form-control-rounded" type="text" value="{{ old('id_number') ? old('id_number') : $user->id_number }}" placeholder="Please enter ID Number" name="id_number">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group mb-3 p-0">
                                            <label>Phone</label>
                                            <input class="form-control form-control-rounded" type="text" value="{{ old('phone') ? old('phone') : $user->phone }}" placeholder="Please enter phone" name="phone">
                                        </div>
                                        <div class="form-group mb-3 p-0">
                                            <label>Birth Date</label>
                                            <input class="form-control form-control-rounded" type="date" value="{{ old('birth_date') ? old('birth_date') : $user->birth_date }}" placeholder="Please enter birth date" name="birth_date">
                                        </div>
                                        <div class="form-group mb-3 p-0">
                                            <label>Profession</label>
                                            <input class="form-control form-control-rounded" type="text" value="{{ old('profession') ? old('profession') : $user->profession }}" placeholder="Please enter profession" name="profession">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group mb-3 p-0">
                                            <label>Profit Share Rate %</label>
                                            <input class="form-control form-control-rounded" type="text" value="{{ old('rate') ? old('rate') : $user->rate }}" placeholder="Please enter profit share rate (0-100)" name="rate">
                                        </div>
                                        <div class="form-group mb-3 p-0">
                                            <label>Password</label>
                                            <input class="form-control form-control-rounded" type="password" placeholder="Leave blank if you don't want to change" name="password">
                                        </div>
                                        <div class="form-group mb-3 p-0">
                                            <label>Please select rank</label>
                                            <select class="form-control form-control-rounded" name="rank">
                                                <option value="1" @if($user->rank == 1) selected @endif >Admin</option>
                                                <option value="2" @if($user->rank == 2) selected @endif >Specialist</option>
                                                <option value="3" @if($user->rank == 3) selected @endif >Assistant</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group mb-3 p-0">
                                            <label>Status</label>
                                            <select class="form-control form-control-rounded" name="status">
                                                <option value="1" @if($user->status == 1) selected @endif >Active</option>
                                                <option value="0" @if($user->status == 0) selected @endif >Passive</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary text-white btn-rounded">Update</button>
                    </form>
                </div>
            </div>
            </div>
        </div>
        <!-- end of row-->
        <!-- end of main-content -->
    </div>
@endsection
