@extends('layouts.app')
@section('title', 'Payments')
@section('content')
    <div class="main-content">
        <div class="breadcrumb">
            <h1 class="mr-2">{{ __('Payments') }}</h1>
            <ul>
                <li><a href="{{ route('home') }}">{{ __('Home') }}</a></li>
                <li>{{ __('Payments') }}</li>
            </ul>
        </div>
        <div class="separator-breadcrumb border-top"></div>
        <!-- CARD ICON-->
        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="card card-icon mb-4">
                    <a href="http://127.0.0.1:8000/appointments/create">
                        <div class="card-body text-center"><i class="i-Add"></i>
                            <p class="text-muted mt-2 mb-2">Total Earnings</p>
                            <p class="line-height-1 text-title text-18 mt-2 mb-0">{{ \App\Models\Payment::sum('amount') }}</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="card card-icon mb-4">
                    <a href="http://127.0.0.1:8000/appointments/list">
                        <div class="card-body text-center"><i class="i-Calendar-4"></i>
                            <p class="text-muted mt-2 mb-2">Total Specialist Earnings</p>
                            <p class="line-height-1 text-title text-18 mt-2 mb-0">{{ \App\Models\SpecialistPayment::sum('earning') }}</p>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6">
                <div class="card card-icon mb-4">
                    <div class="card-body text-center"><i class="i-Calendar"></i>
                        <p class="text-muted mt-2 mb-2">Total Company Earnings</p>
                        <p class="line-height-1 text-title text-18 mt-2 mb-0">{{ \App\Models\Payment::sum('amount') - \App\Models\SpecialistPayment::sum('earning') }}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="card-title">Specialist Payments</div>
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table">
                                    <thead class="thead-light">
                                    <tr>
                                        <th scope="col">Specialist</th>
                                        <th scope="col">Amount</th>
                                        <th scope="col">Date</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse ($specialist_payments as $specialistPayment)
                                        <tr>
                                            <td>{{ ucwords($specialistPayment->specialist->name) }}</td>
                                            @if ($specialistPayment->earning > 0)
                                                <td class="font-weight-bold text-success">+ {{ config('app.money_prefix') }}{{ $specialistPayment->earning }}</td>
                                            @else
                                                <td class="font-weight-bold text-danger">- {{ config('app.money_prefix') }}{{ $specialistPayment->earning }}</td>
                                            @endif
                                            <td>
                                                {{ \Illuminate\Support\Carbon::parse($specialistPayment->created_at)->format('d/m/Y H:i') }}
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td>No payments were made yet.</td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="card-title">Total Payments</div>
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table">
                                    <thead class="thead-light">
                                    <tr>
                                        <th scope="col">Date</th>
                                        <th scope="col">Specialist</th>
                                        <th scope="col">Patient</th>
                                        <th scope="col">Amount</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse ($total_payments as $totalPayment)
                                        <tr>
                                            <td>{{ \Illuminate\Support\Carbon::parse($totalPayment->created_at)->format('d/m/Y H:i') }}</td>
                                            <td>{{ ucwords($totalPayment->specialist->name) }}</td>
                                            <td>{{ ucwords($totalPayment->appointment->patient->name) }}</td>
                                            @if ($totalPayment->amount > 0)
                                                <td class="font-weight-bold text-success">+ {{ config('app.money_prefix') }}{{ $totalPayment->amount }}</td>
                                            @else
                                                <td class="font-weight-bold text-danger">- {{ config('app.money_prefix') }}{{ $totalPayment->amount }}</td>
                                            @endif
                                        </tr>
                                    @empty
                                        <tr>
                                            <td>No payments were made yet.</td>
                                        </tr>
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end of row-->
        <!-- end of main-content -->
    </div>
@endsection
